
<div class="container" id="control">
<div class="row align-items-center">
<?php

$institution_id =  $_GET["inst"];
$assembly_id = $_GET['assembly'];
$category = $_GET['category'];


$sql = "SELECT * FROM category 
WHERE institution_id = '$id_institutions' AND name = '$category'";

$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {

  $stade = $row['stade'];

}

if($stade == 'C'){

// VOTANTES REGISTRADOS EN EL QUORUM
$sql="SELECT COUNT(students_id) AS total_students, SUM(electoral_quotient) AS quotient_students FROM quorum WHERE institution_id=$institution_id";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
 $totalstudent = $row['total_students'];
 $quotient_students = $row['quotient_students']; 

 echo '<div class="col-sm-6">';
 echo '<div class="card">';
 echo '<div class="card-header">',
 '<img src="../../view/img/user.png" alt="" width="40" height="40">'
 ,' Votantes registrados en el Quórum'
 ,'</div>';
 echo '<div class="card-body">',
 $totalstudent
 ,'</div>';
 echo '</div>';
 echo '</div>';
} 

// CONTEO DE VOTANTES
$sql="SELECT COUNT(students_id) AS total, SUM(electoral_quotient) AS controls_quotient
FROM controls WHERE controls.institution_id=$institution_id AND controls.assembly_id=$assembly_id AND controls.category='$category'";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
$totalvotos = $row['total'];
$controls_quotient = $row['controls_quotient'];


 echo '<div class="col-sm-6">';
 echo '<div class="card">';
 echo '<div class="card-header">',
 '<img src="../../view/img/user.png" alt="" width="40" height="40">'
 ,' Votos registrados'
 ,'</div>';
 echo '<div class="card-body">',
 $totalvotos
 ,'</div>';
 echo '</div>';
 echo '</div>';
}

$sql="SELECT votes.assembly_id,
COUNT(votes.candidate_id) AS num_votantes, 
SUM(votes.total_votes) AS TOTAL, 
assemblies.name AS assemblies, 
candidates.name AS candidate,
candidates.last AS candidatelast,
candidates.category AS category,
assemblies.end_date AS end_date,
assemblies.start_date AS start_date,
candidates.type,
candidates.img_candidate,
assemblies.end_date AS end_date,
assemblies.start_date AS start_date,
assemblies.type,
assemblies.img_assembly
FROM votes
INNER JOIN candidates
ON votes.candidate_id=candidates.candidate_id 
AND candidates.category = '$category'
INNER JOIN assemblies 
ON votes.assembly_id=assemblies.assembly_id 
AND votes.institution_id = $institution_id
AND votes.assembly_id = $assembly_id
GROUP BY assemblies, candidate";


$result = $conn->query($sql);
// echo $sql;
// echo $result;
  $i = 0;
  if ($result->num_rows > 0) {
  while ($row = $result->fetch_assoc()) {

    $i++;
    $name_candidate = $row['candidate'];
    $last_candidate = $row['candidatelast'];
    $type = $row['type'];
    $num_votantes = $row['num_votantes'];
    $cuoeficiente = $row['TOTAL']; 

    $total = round((($cuoeficiente*100)/$quotient_students),1);
    $totalvotos = round((($cuoeficiente*100)/$controls_quotient),1);
      
   

        echo '<div class="col-sm-4">';

        echo '<div class="card">';
        echo '<div class="card-footer">',
        // '<img src="../view/img/vote.png" alt="" width="40" height="40">'
        '<img src="data:'.$type.';base64,'.base64_encode($row['img_candidate']).'" alt="" width="110" height="110">'
        ,'</div>';
        echo '<div class="card-header">'
        .$name_candidate.
        ' '
        .$last_candidate.
        '</div>';
        echo '<div class="card-footer bg-transparent border-Secondary">'
        ,'Votos:  '
        .$num_votantes.
        '</div>';
        echo '<div class="card-header">'
        ,'Total coeficiente  '
        .$totalvotos. '%',
        '</div>';
        // echo '<div class="card-footer bg-transparent border-Secondary">'
        // ,'Porcentaje General:  '
        // .$total. '%',
        // '</div>';
        echo '<div class="card-header">'
        ,' '
        .$category.
        '</div>';
        echo '</div>';
        echo '</div>';
        
    }

    require '../../models/student/grafica.php';

 echo '<div class="col-sm-12">'; 
    echo '<hr>';
    echo '</div>';
    echo '<div class="col-sm-6">'; 
    echo '<a type="submit" class="btn btn-outline-danger" data-mdb-ripple-color="dark" target="_blank" href="table_category.php?id='.$id_auditors.'&assembly='.$assembly_id.'&category='.$category.'&inst='.$id_institutions.'">
    <i class="far fa-file-excel"></i>
    Generar tabla de resultados
    </a>';
    echo '</div>';    
    echo '<div class="col-sm-6">'; 
        echo '<a type="submit" class="btn btn-outline-danger" data-mdb-ripple-color="dark"  target="_blank" href="specificvoter.php?id='.$id_auditors.'&assembly='.$assembly_id.'&category='.$category.'&inst='.$id_institutions.'">
        <i class="fas fa-vote-yea"></i>
        Reporte de votantes
        </a>';
    echo '</div>';
    echo '<div class="col-sm-12">'; 
    echo '<hr>';
    echo '<a type="submit" class="btn btn-outline-dark" data-mdb-ripple-color="dark" href="../../view/auditor/category.php?id='.$id_auditors.'&assembly='.$assembly_id.'&inst='.$id_institutions.'">
    <i class="fa fa-arrow-left" aria-hidden="true"></i>
    Atrás
    </a>';
    // echo '<div class="card">';
    // echo '<div class="card-body">'
    // .$name_assembly.
    // '</div>';
    // echo '<div class="card-header">',
    // '<img src="../view/img/vote.png" alt="" width="40" height="40">'
    // ,' Vigente hasta '
    // .$assembly_end.
    // '</div>';
    // echo '</div>';
    echo '</div>';

  }else{

    echo '<div class="col-sm-12">';
    echo '<hr>';

    echo 'NO HAY VOTOS REGISTRADOS EN ESTA CATEGORIA';
    echo '<hr>';
    echo '</div>';
    echo '<div class="col-sm-12">';

    echo '<br>';

    echo '<a type="submit" class="btn btn-outline-dark" data-mdb-ripple-color="dark" href="../../view/auditor/category.php?id='.$id_auditors.'&assembly='.$assembly_id.'&inst='.$id_institutions.'">
    <i class="fa fa-arrow-left" aria-hidden="true"></i>
    Atrás
    </a>';
    echo '</div>';

  }

}elseif($stade == 'A'){

echo 'Esta categoria no esta disponible';

?>

<div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong>Alerta!</strong> Solo podrá ver los resultados, cuando se encuentre cerrada esta categoria
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<?php
    echo '</div>';

echo '<br>';

echo '<hr>';

echo '<a type="submit" class="btn btn-outline-dark" data-mdb-ripple-color="dark" href="../../view/auditor/category.php?id='.$id_auditors.'&assembly='.$assembly_id.'&inst='.$id_institutions.'">
<i class="fa fa-arrow-left" aria-hidden="true"></i>
Atrás
</a>';
echo '</div>';

}


    
?>  




    
</div>
   
   </div>