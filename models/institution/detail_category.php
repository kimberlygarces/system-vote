<div class="container" id="control">
<div class="row align-items-center">

<div class="col-md-12">
           <!-- AREA CHART -->
           <div class="card card-primary"> 
           <!-- PIE CHART -->
           <div class="card card-danger">
                <div class="card-header">
                  <h3 class="card-title">Porcentaje de participación</h3>
                      <div class="card-tools">
                              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                              </button>
                              <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                              </button>
                      </div>
                </div>
                <div class="card-body">
                  <canvas id="pieChart" style="min-height: 150px; height: 150px; max-height: 150px; max-width: 100%;"></canvas>
                </div>
             <!-- /.card-body -->
           </div>
           <!-- /.card -->
         </div>
         </div>
<?php

$institution_id =  $_GET["id"];
$assembly_id = $_GET['assembly'];
$category = $_GET['category'];
// CONTEO DE ESTUDIANTES
$sql="SELECT COUNT(students_id) AS total_students, SUM(electoral_quotient) AS quotient_students FROM students WHERE institution_id=$institution_id";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
 $totalstudent = $row['total_students'];
 $quotient_students = $row['quotient_students']; 

 echo '<div class="col-sm-4">';
 echo '<div class="card">';
      echo '<div class="card-header">',
      '<img src="../view/img/user.png" alt="" width="40" height="40">'
      ,' Votantes habilitados '
      ,'</div>';
      echo '<div class="card-body">',
      $totalstudent
      ,'</div>';
 echo '</div>';
 echo '</div>';
} 



// CONTEO DE QUÓRUN
$sql="SELECT COUNT(students_id) AS total_quorun, SUM(electoral_quotient) AS quotient_quorun FROM quorum WHERE institution_id=$institution_id";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
 $total_quorun = $row['total_quorun'];
 $quotient_quorun = round(($row['quotient_quorun']),1); 


 echo '<div class="col-sm-4">';
 echo '<div class="card">';
      echo '<div class="card-header">',
      '<img src="../view/img/asistence.png" alt="" width="40" height="40">'
      ,' Quórum registrado'
      ,'</div>';
      echo '<div class="card-body">',
      $total_quorun,' coeficiente(',$quotient_quorun,')'
      ,'</div>';
 echo '</div>';
 echo '</div>';
} 

// CONTEO DE VOTANTES
$sql="SELECT COUNT(students_id) AS total, SUM(electoral_quotient) AS controls_quotient
FROM controls WHERE institution_id=$institution_id  AND category='$category'";

$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
$totalvotosstudent = $row['total'];
$controls_quotient = $row['controls_quotient'];

 echo '<div class="col-sm-4">';
 echo '<div class="card">';
    echo '<div class="card-header">',
    '<img src="../view/img/vote.png" alt="" width="40" height="40">'
    ,' Votos registrados'
    ,'</div>';
    echo '<div class="card-body">',
    $totalvotosstudent
    ,'</div>';
 echo '</div>';
 echo '</div>';

}

// mostrar los resusltados por candidato
$totalvotes = $total_quorun - $totalvotosstudent;
$sql="SELECT votes.assembly_id,
COUNT(votes.candidate_id) AS num_votantes, 
SUM(votes.total_votes) AS TOTAL, 
assemblies.name AS assemblies, 
candidates.candidate_id AS candidate_id,
candidates.name AS candidate,
candidates.last AS candidatelast,
candidates.category AS category,
candidates.type,
candidates.img_candidate
FROM votes
INNER JOIN candidates
ON votes.candidate_id=candidates.candidate_id 
AND candidates.category = '$category'
INNER JOIN assemblies 
ON votes.assembly_id=assemblies.assembly_id 
AND votes.institution_id = $institution_id
AND votes.assembly_id = $assembly_id
GROUP BY assemblies, candidate";


$result = $conn->query($sql);
  $i = 0;
  if ($result->num_rows > 0) {
  while ($row = $result->fetch_assoc()) {

    $i++;
    $name_candidate = $row['candidate'];
    $last_candidate = $row['candidatelast'];
    $type = $row['type'];
    $num_votantes = $row['num_votantes'];
    $cuoeficiente = $row['TOTAL']; 

    $candidate_id = $row['candidate_id']; 


   

    $total = round((($cuoeficiente*100)/$quotient_quorun),1);
    $totalvotos = round((($cuoeficiente*100)/$controls_quotient),1);
      
   

        echo '<div class="col-sm-4">';

        echo '<div class="card">';
        echo '<div class="card-footer">',
        // '<img src="../view/img/vote.png" alt="" width="40" height="40">'
        '<img src="data:'.$type.';base64,'.base64_encode($row['img_candidate']).'" alt="" width="110" height="110">'
        ,'</div>';
        echo '<div class="card-header">'
        .$name_candidate.
        ' '
        .$last_candidate.
        '</div>';
        echo '<div class="card-footer bg-transparent border-Secondary">'
        ,'Número de votos:  '
        .$num_votantes.
        '</div>';
        echo '<div class="card-header">'
        ,'Total coeficiente '
        .$cuoeficiente.' ('
        .$totalvotos. '%)',
        '</div>';
        // echo '<div class="card-footer bg-transparent border-Secondary">'
        // ,'Porcentaje Quórun:  '
        // .$total. '%',
        // '</div>';
        echo '<div class="card-footer bg-transparent border-Secondary">'
        ,' '
        .$category.
        '</div>';
        echo '</div>';
        echo '</div>';
        
    }

    echo '<div class="col-sm-12">'; 
    echo '<hr>';
    echo '</div>';
function validate($assembly_id,$candidate_id,$institution_id,$conn)
{   
$controls = "SELECT * FROM `votes` 
WHERE candidate_id = '" . $candidate_id. "'
AND institution_id = '$institution_id'
AND assembly_id = '$assembly_id'
";

$control = $conn->query($controls);
   
// var_dump($control->fetch_assoc()['assembly_id']);
if ($control->num_rows > 0) {
    return $control->fetch_assoc()['candidate_id'];
}
return null;
}
$sql="SELECT * FROM `candidates` WHERE institution_id=$institution_id  AND category='$category'";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
$candidate_general = $row['candidate_id']; 
$name_candidate = $row['name']; 
$last_candidate  = $row['last']; 
$category_name_genera = $row['category'];
$type = $row['type'];


if(validate($assembly_id,$candidate_general,$institution_id,$conn) == null){  

  echo '<div class="col-sm-4">';
  echo '<div class="card">';
  echo '<div class="card-footer">',
  // '<img src="../view/img/vote.png" alt="" width="40" height="40">'
  '<img src="data:'.$type.';base64,'.base64_encode($row['img_candidate']).'" alt="" width="110" height="110">'
  ,'</div>';
  echo '<div class="card-header">'
  .$name_candidate.
  ' '
  .$last_candidate.
  '</div>';
  echo '<div class="card-footer bg-transparent border-Secondary">'
  ,'Número de votos: 0 ',
  '</div>';
  echo '<div class="card-header">'
  ,'Total coeficiente 0 ',
  '</div>';
  echo '<div class="card-footer bg-transparent border-Secondary">'
  ,' '
  .$category.
  '</div>';
  echo '</div>';
  echo '</div>';
      
  }
}

    echo '<div class="col-sm-12">'; 
    echo '<hr>';
    echo '</div>';

      echo '<div class="col-sm-6">'; 
      echo '<a type="submit" class="btn btn-outline-danger" data-mdb-ripple-color="dark" href="specificassembly.php?id='.$institution_id.'&assembly='.$assembly_id.'&category='.$category.'">
      <i class="far fa-file-excel"></i>
      Generar tabla de resultados
      </a>';
      echo '</div>';

      echo '<div class="col-sm-6">'; 
          echo '<a type="submit" class="btn btn-outline-danger" data-mdb-ripple-color="dark" href="specificvoter.php?id='.$institution_id.'&assembly='.$assembly_id.'&category='.$category.'">
          <i class="fas fa-vote-yea"></i>
          Reporte de votantes
          </a>';
      echo '</div>';
      echo '</div>';

    echo '<div class="col-sm-12">'; 
    echo '<hr>';
    echo '<a type="submit" class="btn btn-outline-dark" data-mdb-ripple-color="dark" href="detail.php?id='.$institution_id.'&assembly='.$assembly_id.'">
    <i class="fa fa-arrow-left" aria-hidden="true"></i>
    Atrás
    </a>';
    echo '</div>';  
  }else{

    echo '<div class="col-sm-12">';
    echo '<hr>';
    echo 'NO HAY VOTOS REGISTRADOS EN ESTA CATEGORIA';
    echo '<hr>';
    echo '</div>';
    echo '<div class="col-sm-12">';
    echo '<br>';

    echo '<a type="submit" class="btn btn-outline-dark" data-mdb-ripple-color="dark" href="detail.php?id='.$institution_id.'&assembly='.$assembly_id.'">
    <i class="fa fa-arrow-left" aria-hidden="true"></i>
    Atrás
    </a>';
    echo '</div>';

  }  

  
    ?>  
    </div>
   
</div>
</section>


<script src="plugins/chart.js/Chart.min.js"></script>

<?php require 'template_footer.php'?>



<script type="text/javascript">
  $(function () { 

    var donutChartCanvas = $('#pieChart').get(0).getContext('2d')

    $totalvotes='<?php echo $totalvotes?>';
    $totalvotos = '<?php echo $totalvotosstudent?>';


    var donutData = {
      labels: [
          'Votos Registrados',
          'Usuario registrado sin voto',

      ],
      datasets: [
        {
          data: [$totalvotos,$totalvotes],
          backgroundColor : ['#f56954', '#00a65a'],
        }
      ]
    }

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieData        = donutData;
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })

  })
</script>


<script type="text/javascript">
  $(function () { 

    var donutChartCanvas = $('#pieCharttotal').get(0).getContext('2d')

    $totalstudent='<?php echo $totalstudent?>';
    $totalvotos = '<?php echo $num_votantes?>';


    var donutData = {
      labels: [
          'Votantes habilitados',
          'Votos registrados',

      ],
      datasets: [
        {
          data: [$totalvotos,$totalstudent],
          backgroundColor : ['#f56954', '#00a65a'],
        }
      ]
    }

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieCharttotal').get(0).getContext('2d')
    var pieData        = donutData;
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })

  })
</script>




