<?php
$institution_id =  $_GET["id"];
$assembly_id = $_GET['assembly'];
$category = $_GET['category'];
//MUMERO DE ESTUDIANTES HABILITADOS PARA VOTAR
$sql="SELECT COUNT(students_id) AS total_students, SUM(electoral_quotient) AS quotient_students FROM quorum WHERE institution_id=$institution_id";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
  $totalstudent = $row['total_students'];
  $quotient_students = $row['quotient_students']; 
} 
$sql="SELECT 
COUNT(controls.students_id) AS total,
SUM(controls.electoral_quotient) AS controls_quotient,
controls.category
FROM controls 
WHERE controls.institution_id=$institution_id 
AND controls.assembly_id=$assembly_id 
AND controls.category = '$category'
";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
$totalvotos = $row['total'];
$controls_quotient = $row['controls_quotient'];

}

//se realiza la consulta para mostrar el numero de votos por candidato
$sql="SELECT votes.assembly_id,
COUNT(votes.candidate_id) AS num_votantes, 
SUM(votes.total_votes) AS TOTAL, 
assemblies.name AS assemblies, 
candidates.name AS candidate,
candidates.last AS candidatelast,
candidates.category AS category,
candidates.type,
candidates.img_candidate
FROM votes
INNER JOIN candidates
ON votes.candidate_id=candidates.candidate_id 
AND candidates.category = '$category'
INNER JOIN assemblies 
ON votes.assembly_id=assemblies.assembly_id 
AND votes.institution_id = $institution_id
AND votes.assembly_id = $assembly_id
GROUP BY assemblies, candidate";

$result = $conn->query($sql);
  $i = 0;
  $voto = 0;
  echo' <table id="example1" class="table table-bordered table-striped">';
  echo' <thead>';
  echo'  <tr>';
  // echo'  <th scope="col">Id</th>';
  echo'  <th scope="col"></th>';
  echo'  <th scope="col">Candidato</th>';
  echo'  <th scope="col">Categoría</th>';
  echo'  <th scope="col">Número de votos</th>';
  echo'  <th scope="col">Total coeficiente</th>';
  echo'  <th scope="col">Porcentaje coeficiente</th>';
  echo'  </tr>';
  echo'</thead>';
  echo' <tbody>';
  while ($row = $result->fetch_assoc()) {
    $i++;
    $name_assembly = $row['assemblies'];
    $last_candidate = $row['candidatelast'];
    $name_candidate = $row['candidate'] ;
    $category = $row['category'] ;
    $num_votantes = $row['num_votantes'];
    $cuoeficiente = $row['TOTAL']; 

   
    $total = round(($controls_quotient),2);
    $totalvotos = round((($cuoeficiente*100)/$controls_quotient),1);
      

    $type = $row['type'];

    echo'<tr>';
    // echo '<td>' . $i. '</td>';
    echo '<td>','<img src="data:'.$type.';base64,'.base64_encode($row['img_candidate']).'" alt="" width="40" height="40">','</td>';
    echo '<td>' . $name_candidate . ' ' .$last_candidate.'</td>';
    echo '<td>' . $category. '</td>';
    echo '<td>' . $num_votantes. '</td>';    
    echo '<td>' . $total.'</td>';    
    echo '<td>' . $totalvotos. ' %</td>';       
   
   
    }
    echo'</tbody>';
    echo' </table>'; 
    
    echo '<a type="submit" class="btn btn-outline-dark" data-mdb-ripple-color="dark" href="detail_category.php?id='.$institution_id.'&assembly='.$assembly_id.'&category='.$category.'">
    <i class="fa fa-arrow-left" aria-hidden="true"></i>
    Atrás
    </a>';
    echo '</div>';
     
?>  


