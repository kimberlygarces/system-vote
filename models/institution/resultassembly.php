<?php
$institution_id =  $_GET["id"];


  //MUMERO DE ESTUDIANTES HABILITADOS PARA VOTAR

  $sql="SELECT 
  COUNT(students.students_id) AS total_students,
  SUM(students.electoral_quotient) AS quotient_students,
    students.name 
   FROM students 
   WHERE students.institution_id=$institution_id
   ";


$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
  $totalstudent = $row['total_students'];
  $quotient_students = $row['quotient_students']; 

} 

$sql="SELECT 
COUNT(controls.students_id) AS total,
SUM(controls.electoral_quotient) AS controls_quotient
FROM controls 
WHERE controls.institution_id=$institution_id ";


$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
$totalvotos = $row['total'];
$controls_quotient = $row['controls_quotient'];

}


//se realiza la consulta para mostrar el numero de votos por candidato
$sql="SELECT votes.assembly_id,
-- COUNT(votes.candidate_id) AS TOTAL, 
SUM(votes.total_votes) AS TOTAL, 
assemblies.name AS assemblies, 
candidates.name AS candidate,
candidates.last AS candidatelast,
candidates.	category AS category,
assemblies.end_date AS end_date,
assemblies.start_date AS start_date,
candidates.type,
candidates.img_candidate
FROM votes
INNER JOIN candidates 
ON votes.candidate_id=candidates.candidate_id 
INNER JOIN assemblies 
ON votes.assembly_id=assemblies.assembly_id 
AND votes.institution_id = $institution_id
GROUP BY assemblies, candidate";

$result = $conn->query($sql);
  
  $i = 0;
  $voto = 0;
  echo' <table id="example1" class="table table-bordered table-striped">';
  echo' <thead>';
  echo'  <tr>';
  // echo'  <th scope="col">Id</th>';
  echo'  <th scope="col"></th>';
  echo'  <th scope="col">Candidato</th>';
  echo'  <th scope="col">Categoria</th>';
  // echo'  <th scope="col">Asamblea</th>';
  echo'  <th scope="col">General</th>';
  echo'  <th scope="col">Resultado</th>';

  echo'  </tr>';
  echo'</thead>';
  echo' <tbody>';
  while ($row = $result->fetch_assoc()) {
    $i++;
    $name_assembly = $row['assemblies'];
    $last_candidate = $row['candidatelast'];
    $name_candidate = $row['candidate'] ;
    $category = $row['category'] ;
    $start_date = $row['start_date'];
    $end_date = $row['end_date'] ;
    $cuoeficiente = $row['TOTAL']; 

    $total = round((($cuoeficiente*100)/$quotient_students),1);
    $totalvotos = round((($cuoeficiente*100)/$controls_quotient),1);



    $type = $row['type'];

    echo'<tr>';
    // echo '<td>' . $i. '</td>';
    echo '<td>','<img src="data:'.$type.';base64,'.base64_encode($row['img_candidate']).'" alt="" width="40" height="40">','</td>';
    echo '<td>' . $name_candidate . ' ' .$last_candidate.'</td>';
    echo '<td>','<a href="#" data-toggle="popover" title="Detalle de Asamblea" data-content=" Asamble activa del '.$start_date.'  hasta  '.$end_date.'">' . $category . '</a>' ,'</td>';

    echo '<td>' . $total.'(%)','</td>';  
    echo '<td>' . $totalvotos. '</td>';
     
    }
    echo'</tbody>';
    echo' </table>';  
     
?>  


