
<div class="row align-items-center">

<div class="col-md-12">
           <!-- AREA CHART -->
           <div class="card card-primary"> 
           <!-- PIE CHART -->
           <div class="card card-danger">
                <div class="card-header">
                  <h3 class="card-title">Porcentaje de participación % </h3>
                      <div class="card-tools">
                              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                              </button>
                              <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                              </button>
                      </div>
                </div>
                <div class="card-body">
                  <canvas id="pieChart" style="min-height: 150px; height: 150px; max-height: 150px; max-width: 100%;"></canvas>
                </div>
             <!-- /.card-body -->
           </div>
           <!-- /.card -->
         </div>
         </div>
<?php
$institution_id =  $_GET["id"];

// CONTEO DE ESTUDIANTES
$sql="SELECT COUNT(students_id) AS total_students, SUM(electoral_quotient) AS quotient_students FROM students WHERE institution_id=$institution_id";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
 $totalstudent = $row['total_students'];
 $quotient_students = $row['quotient_students']; 

 echo '<div class="col-sm-6">';
 echo '<div class="card">';
      echo '<div class="card-header">',
      '<img src="../view/img/user.png" alt="" width="40" height="40">'
      ,' Votantes habilitados '
      ,'</div>';
      echo '<div class="card-body">',
      $totalstudent
      ,'</div>';
 echo '</div>';
 echo '</div>';
} 

// CONTEO DE QUÓRUN
$sql="SELECT COUNT(students_id) AS total_quorun, SUM(electoral_quotient) AS quotient_quorun FROM quorum WHERE institution_id=$institution_id";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
 $total_quorun = $row['total_quorun'];
 $quotient_quorun = $row['quotient_quorun']; 
 $porcentaje_quorum =  round(($quotient_quorun*100/$quotient_students), 2); //PORCENTAJE DE QUORUN TOTAL DE VOTANTES REGISTRADOS
 $porcentaje_voter =  round(($total_quorun*100/$totalstudent), 2); //PORCENTAJE DE QUORUN SEGUN COEFICIENTE DE VOTANTES REGISTRADOS


 echo '<div class="col-sm-6">';
 echo '<div class="card">';
      echo '<div class="card-header">',
      '<img src="../view/img/asistence.png" alt="" width="40" height="40">'
      ,'Quórum registrado'
      ,'</div>';
      echo '<div class="card-body">',
      ' ',
      $total_quorun,'(',
      $porcentaje_quorum,'%) '

      ,'</div>';
 echo '</div>';
 echo '</div>';
} 
$totalvotes = $totalstudent - $total_quorun;



?>
