<div class="col-sm-12">
<div class="card-header">
    Resultados Generales
</div>
<?php

$institution_id =  $_GET["inst"];
$assembly_id = $_GET['assembly'];
$category = $_GET['category'];


require '../../controllers/conection.php';

$sql="SELECT votes.assembly_id,
COUNT(votes.candidate_id) AS num_votantes, 
SUM(votes.total_votes) AS TOTAL, 
assemblies.name AS assemblies, 
candidates.name AS candidate,
candidates.last AS candidatelast,
candidates.category AS category
FROM votes
INNER JOIN candidates
ON votes.candidate_id=candidates.candidate_id 
AND candidates.category = '$category'
INNER JOIN assemblies 
ON votes.assembly_id=assemblies.assembly_id 
AND votes.institution_id = $institution_id
AND votes.assembly_id = $assembly_id
GROUP BY assemblies, candidate";
$result = $conn->query($sql);
?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Language', 'Rating'],
      <?php
      if($result->num_rows > 0){
          while($row = $result->fetch_assoc()){
            echo "['".$row['candidate'].' '.$row['candidatelast']."', ".$row['TOTAL']."],";
          }
      }
      ?>
    ]);
    
    var options = {
        title: '',
        width: 1000,
        height: 700,
    };
    
    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
    
    chart.draw(data, options);
}
</script>

    <!-- Display the pie chart -->
    <div id="piechart"></div>
    
    </div>

