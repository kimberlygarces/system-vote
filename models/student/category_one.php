<?php
$assemply_id =  $_GET["assembly"];
$hoy = date("Y-m-d"); 
$students_id;
$id_institutions;
   
function validate($assembly,$student,$institution,$category,$conn)
        {   
        $controls = "SELECT * FROM `controls` 
        WHERE students_id = '" . $student. "'
        AND institution_id = '$institution'
        AND assembly_id = '$assembly'
        AND category = '$category'";
        $control = $conn->query($controls);   
        // var_dump($control->fetch_assoc()['assembly_id']);
        if ($control->num_rows > 0) {
            return $control->fetch_assoc()['category'];
        }
        return null;
        }

   
    function validate_quorum($assembly,$student,$institution,$category,$conn)
    {   
                $controls = "SELECT * FROM `quorum` 
                WHERE students_id = '" . $student. "'
                AND institution_id = '$institution'
                AND assembly_id = '$assembly'
                ";
                $controls = $conn->query($controls);
                  
                // var_dump($control->fetch_assoc()['assembly_id']);
                if ($controls->num_rows > 0) {
                    return $controls->fetch_assoc()['students_id'];
                }

                return null;

      }




      $sql = "SELECT * FROM assemblies 
        WHERE institution_id = '$id_institutions'";

        $result = $conn->query($sql);
        while ($row = $result->fetch_assoc()) {
        $assembly_id = $row['assembly_id'];  
        $start_date = $row['start_date'];
        $end_date = $row['end_date'];
        $start_time = $row['end_time'];
        $end_time = $row['end_time'];

        $hoy = date("Y-m-d"); 
        $stade = $row['stade'];
        $type =$row['type'];

        if ($stade == "P") {

          echo '<div class="col-sm-12">'; 
          echo '<div class="card">';
          echo '<div class="card-body">'
          ,'LA ASAMBLEA SE ESTA PREPARANDO',
          '</div>';
    
          echo '</div>';
          echo '</div>';

        }elseif ($stade == "A") {

//consulta para mostrar los candidatos a los estudiantes - solo si pertenecen al mismo grupo
      // echo $user;  
      // echo $id_institutions;   
        $sql="SELECT `category_id`,
        `assembly_id`, 
        `institution_id`, 
        `name` AS name_category, 
        `stade`, 
        `description` 
        FROM `category` WHERE assembly_id = '$assemply_id'
        ";

      $result = $conn->query($sql);
        $ia = 0;
        echo '<div class="row align-items-center">';
        while ($row = $result->fetch_assoc()) {
          $category = $row['name_category']; 
          $assembly_id = $row['assembly_id'];           
          $stade = $row['stade'];

         if(validate_quorum($assembly_id,$students_id,$id_institutions,$category,$conn) == null){
          if ($stade == "A") {
            
            echo '<div class="col-sm-6">'; 
            echo '<div class="card">';

                echo '<div class="card-header">',
                '<img src="../img/vote.png" alt="" width="40" height="40">'
                .$category.
                '</div>';

                echo '<div class="card-body">',
                '<p> ','No estas inscrito en el quórum','</p>',
                '<a class="btn btn-light" data-mdb-ripple-color="dark" href="category2.php?category='.$row["name_category"].'&assembly='.$assembly_id.'&id='.$students_id.'">
                <i class="fa fa-users fa-2x" aria-hidden="true"></i>
                ver candidatos </a>',
                '</div>';

            echo '<br>';
            echo '</div>';        
            echo '</div>';        

          }else{

            echo '<div class="col-sm-6">'; 
          echo '<div class="card">';
            echo '<div class="card-header">',
            '<img src="../img/vote.png" alt="" width="40" height="40">'
            .$category.
            '</div>';           
            echo '<div class="card-body">',
            '<p> ',' Categoria cerrada','</p>',
            '<a href="detail_category.php?assembly='.$assembly_id.'&id='.$students_id.'&category='.$category.'&inst='.$id_institutions.'" type="button" class="btn btn-light">','<i class="fa fa-pie-chart fa-2x" aria-hidden="true"></i>  Ver resultados','</a>',
            '</div>';
          echo '<br>';
          echo '</div>';        
          echo '</div>';    


          }
        }

      if(validate_quorum($assembly_id,$students_id,$id_institutions,$category,$conn) == $students_id){

        if(validate($assembly_id,$students_id,$id_institutions,$category,$conn) == $category && $stade == "A"){
        
            echo '<div class="col-sm-6">'; 
            echo '<div class="card">';

                echo '<div class="card-header">',
                '<img src="../img/vote.png" alt="" width="40" height="40">'
                .$category.
                '</div>';

                echo '<div class="card-body">',
                '<p> ',' Ya realizaste tu voto','</p>',
                '<i class="fa fa-check" aria-hidden="true"></i>   voto registrado correctamente </a>',
                '</div>';

            echo '<br>';
            echo '</div>';        
            echo '</div>';        

        }else{          

        if ($stade == "A") {

          echo '<div class="col-sm-6">'; 
          echo '<div class="card">';

              echo '<div class="card-header">',
              '<img src="../img/vote.png" alt="" width="40" height="40">'
              .$category.
              '</div>';

              echo '<div class="card-body">',
              '<p> ','Ver los candidatos','</p>',
              '<a class="btn btn-light" data-mdb-ripple-color="dark" href="category2.php?category='.$row["name_category"].'&assembly='.$assembly_id.'&id='.$students_id.'">
              <i class="fa fa-users fa-2x" aria-hidden="true"></i>
              votar </a>',
              '</div>';

            echo '<br>';
            echo '</div>';        
            echo '</div>';        



        }elseif ($stade == "I") {
    
          echo '<div class="col-sm-6">'; 
          echo '<div class="card">';

              echo '<div class="card-header">',
              '<img src="../img/vote.png" alt="" width="40" height="40">'
              .$category.
              '</div>';

              echo '<div class="card-body">',
              '<p> ',' No se puede acceder a esta categoria','</p>',
              '<i class="fa fa-check" aria-hidden="true"></i>   Categoria desactivada </a>',
              '</div>';

            echo '<br>';
            echo '</div>';        
            echo '</div>';        


         }elseif ($stade == "C") {   

          echo '<div class="col-sm-6">'; 
          echo '<div class="card">';


            echo '<div class="card-header">',
            '<img src="../img/vote.png" alt="" width="40" height="40">'
            .$category.
            '</div>';

           
            echo '<div class="card-body">',
            '<p> ',' Categoria cerrada','</p>',
            '<a href="detail_category.php?assembly='.$assembly_id.'&id='.$students_id.'&category='.$category.'&inst='.$id_institutions.'" type="button" class="btn btn-light">','<i class="fa fa-pie-chart fa-2x" aria-hidden="true"></i>  Ver resultados','</a>',
            '</div>';
          echo '<br>';
          echo '</div>';        
          echo '</div>';    

         }   
      }
    }
  } 
  }
        }

          echo '<div class="col-sm-12">'; 
          echo '<a type="submit" class="btn btn-raised btn-danger" href="../student.php?id='.$students_id.'"
          <i class="fa fa-check-circle" aria-hidden="true"></i>
          Terminar</a>';
          // echo '<button type="submit" class="btn btn-raised btn-danger"><i class="fas fa-user-plus"></i> Terminar</button>';  
          echo '<hr>';
          echo '<div class="card">';
          echo '<div class="card-body">'
          .$name_assembly.
          '</div>';
          echo '<div class="card-header">',
          '<img src="../img/reloj.png" alt="" width="40" height="40">'

          // '<img src="data:'.$type.';base64,'.base64_encode($row['img_assembly']).'" alt="" width="40" height="40">'
          ,' Vigente hasta '
          .$assembly_end.

          '</div>';
          echo '</div>';
          echo '</div>';


?>                     