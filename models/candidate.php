<?php

require '../controllers/conection.php';

if(!empty($_POST["institution_id"]) and empty($_POST["Usuario"])){
  $institution_id = $_POST["institution_id"] ;
  echo '<p>'.$institution_id.'</p>';
  // echo $_POST["Usuario"];
  ?>
  
<!-- formulario para la validacion del usuario -->
  <form action="../controllers/validacion/validacion.php" method="post" enctype="multipart/form-data">
    <div class="col-sm-12 inscriction">
        <!-- <input type="hidden" id="Usuario" name="Usuario" value="Usuario"> -->
        <input type="hidden" id="institution_id" name="institution_id" value="<?php echo "".$institution_id."";?>">
        <div class="col-sm-12">
          <div  class='row'>
          <div class="col-sm-4">
          <label for="usr">Tipo doc</label>
          <select name="typedoc" id="typedoc" class="form-control">
          <option value="CC">CC</option>
          <option value="PS">CE</option>
          </select>
          </div>
          <div class="col-sm-8">
          <label for="usr">Número de documento</label>
          <input name="documentoInscrito" type="text" class="form-control" id="documentoInscrito" required>
          </div>

          </div>
        </div>
        <hr>
        <button type="submit" class="btn validacion-btn" >
        Validación de usuario
        </button>

    </div>
</form>
<!-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

<?php
// condición cuando se valida el usuario

  }elseif(!empty($_GET["id"])){
    $institution_id = $_GET["inst"] ;
  echo <<<END
  <script type="text/javascript">
  function sayHi() {
    Swal.fire({
      title: 'Ya realizó la actualización de datos?',
      icon: 'question',
      showDenyButton: true,
      confirmButtonText: `No`,
      denyButtonText: `Si`,
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire(
        {
          icon: 'warning',
          title: 'Debe ingresar al siguiente enlace para la actualización de datos',
          showConfirmButton: false,
          timer: 9500,
          footer: '<a href="https://www.google.com/" target="_blank">Actualización de datos</a>'
      
        }

        )
      }
    })

  }
   setTimeout(sayHi, 1000);
  </script>
  
  END;
  ?>

<input type="hidden" id="Usuario" name="Usuario" value="Usuario">
<input type="hidden" id="institution_id" name="institution_id" value="<?php echo "".$institution_id."";?>">
<p><?php echo $institution_id ?></p>

<!-- formulario para el registro de datos del candidato -->
  <form action="../controllers/register/register_candidate.php"" method="post" enctype="multipart/form-data">
  <input name="institution_id" type="hidden" class="form-control" id="institution_id" value="<?php echo $institution_id ?>">

  <div  class='row'>
  <div class="col-xs-12 col-md-12">
         <p> Datos Personales</p>
         <hr>

  </div>
        <div class="col-xs-12 col-md-6">
            <label for="usr">Primer Nombre</label>
            <input name="namecandidate" type="text" class="form-control" id="nombre1Inscrito" required>
        </div>
        <div class="col-xs-12 col-md-6">
            <label for="usr">Segundo Nombre</label>
            <input name="namecandidate2" type="text" class="form-control" id="nombre2Inscrito" required>
        </div>

        <div class="col-xs-12 col-md-6">
          <label for="usr">Primer Apellido</label>
          <input name="lastcandidate" type="text" class="form-control" id="apellido1Inscrito" required>
        </div>
        <div class="col-xs-12 col-md-6">
          <label for="usr">Segundo Apellido</label>
          <input name="lastcandidate2" type="text" class="form-control" id="apellido2Inscrito" required>
        </div>
        <div class="col-xs-12 col-md-6">
          <label for="usr">Lugar de nacimiento</label>
          <input name="place_birth" type="text" class="form-control" id="nacimientoInscrito" required>
        </div>
        <div class="col-xs-12 col-md-6">
          <label for="usr">fecha de nacimiento</label>
          <input name="datebirth" type="date" class="form-control" id="fechaInscrito" required>
        </div>


    <div class="col-xs-12 col-md-6">
      <div class="form-group">
        <label for="exampleFormControlSelect1">Genero</label>
        <select class="form-control" name="gender" id="GeneroInscrito">
          <option>masculino</option>
          <option>femenino</option>
        </select>
      </div>
    </div>





        <div class="col-xs-12 col-md-12">
        <p>Datos de Identificación</p>
          <hr>
        </div>

        <div class="col-sm-8">
          <div  class='row'>
          <div class="col-sm-4">
          <label for="usr">Tipo Documento</label>
          <select name="typedoc" id="typedoc" class="form-control">
          <option value="CC">CC</option>
          <option value="CE">CE</option>
          </select>
          </div>
          <div class="col-sm-8">
          <label for="usr">Número de documento</label>
          <input name="documentcandidate" type="text" class="form-control" id="documentInscrito" required>
          </div>

          </div>
          </div>
          <input name="gradecandidate" type="hidden" class="form-control" id="documentfechaInscrito" value="1233">

        <div class="col-xs-12 col-md-4">            
            <label for="pwd">fecha de expedición</label>
         
           <input name="document_date" type="date" class="form-control" id="documentfechaInscrito" required>
        </div>
        <div class="col-xs-12 col-md-6">            
            <label for="pwd">Lugar de Expedición</label>
            <input name="lugar_date" type="text" class="form-control" id="lugarInscrito" required>
        </div>
        <div class="col-sm-8">
        <hr>

            <label for="usr">Tarjeta Profesional</label>
            <input class='form-control'  type='text' name='professional_card' id="tarjetaInscrito" required>
        </div>  
        
        <div class="col-xs-12 col-md-4">  
        <hr>
          
      <div class="form-group">
        <label for="exampleFormControlSelect1">Autorizado o Titilado</label>
        <select class="form-control" name="AT" id="AT_Inscrito">
          <option>A</option>
          <option>T</option>
        
        </select>
      </div>         
    </div>

        
        <div class="col-xs-12 col-md-12">
          <br>
        <p>Datos de contacto</p>
          <hr>
        </div>
        <div class="col-xs-12 col-md-6">
            <label for="usr">Correo </label>
            <input name="correo" type="text" class="form-control" id="correoInscrito" required>
        </div>


        <div class="col-xs-12 col-md-6">
            <label for="pwd">Telefono Movil</label>
          <input name="movil" type="text" class="form-control" id="telefonoCInscrito" required>            
        </div>
        
        <div class="col-xs-12 col-md-6">
            <label for="pwd">Telefono Fijo </label>
          <input name="fijo" type="text" class="form-control" id="telefonoFInscrito" required>            
        </div>
        
        <div class="col-xs-12 col-md-6">
            <label for="pwd">Ciudad de Residencia</label>
          <input name="residencia" type="text" class="form-control" id="ciudadInscrito" required>            
        </div>
               
        <div class="col-xs-12 col-md-6">
            <label for="pwd">Dirección de residencia</label>
          <input name="direction" type="text" class="form-control" id="ciudadInscrito" required>            
        </div>

        <?php

        if($institution_id=="Para candidatos a representante de Instituciones Educativas"){
          // echo $institution_id;
          ?>

          <div class="col-xs-12 col-md-12">
            <hr>
                <label for="pwd">Código SNIES</label>
                <select class="form-control" name="SNIES" id="AT_Inscrito">
                <option>1233A</option>
                <option>1233A</option>
              
              </select>
                <!-- <input name="SNIES" type="text" class="form-control" id="CodigoInscrito" required>             -->
          </div>
        <?php

        }
        ?>

        


<hr>
<div class="col-xs-12 col-md-12"><hr>
<button type="button" id="btn-siguiente" class="btn btn-raised btn-success" data-toggle="modal" data-target="#exampleModalScrollable">
Siguiente
</button>

</div>
<!-- Modal -->
<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Leer declaración para continuar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="alert alert-warning alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <i class="fa fa-check" aria-hidden="true"></i>
    Debe selecciónar las casillas para continuar
  </div>
      <ul class="terminos">
  <li>
  <input class="form-check-input" type="checkbox" value="" id="checkbox">

  	Declaro, bajo la gravedad de juramento, que la información consignada en el presente formulario, es correcta y es fiel expresión de la verdad; me obligo a entregar información veraz y verificable, de conformidad con el artículo 83 de la Constitución Nacional. Este campo es obligatorio. 
  </li>
  <li>
  <input class="form-check-input" type="checkbox" value="" id="checkbox">

  Acepto tratamiento de datos personales (Con el fin de dar cumplimiento a lo previsto en la Ley 1581 de 2012 reglamentado por el Decreto 1377 de 2013, mediante la firma del presente documento, autorizo de manera libre, expresa, voluntaria, inequívoca y debidamente informada, a la UAE Junta central de Contadores para el manejo (recolección, recaudo, almacenamiento, uso, circulación, supresión, procesamiento, compilación, intercambio, tratamiento, actualización y disposición) de los datos aquí contenidos y que se incorporen en distintas bases, bancos de datos, o en repositorios electrónicos de todo tipo con que cuenta la Entidad. Esta información será utilizada en mi beneficio para el desarrollo de las funciones propias de la Entidad, como ejercer la inspección y vigilancia en el ejercicio de la Contaduría Pública. Soy consciente que podré ejercer mis derechos en cuanto a: a) Conocer, actualizar y rectificar mis Datos Personales; b) Revocar la autorización y/o solicitar la supresión del Dato Personal. En todo caso y para el ejercicio de mis derechos podré manifestarlo por escrito a la cuenta de correo electrónico dispuesta para este efecto: elecciones@jcc.gov.co o mediante carta dirigida a la dirección: Carrera 16 No 97 - 46 Oficina 301   en Bogotá.). Es opcional seleccionar
  </li>
  <li>
  <input class="form-check-input" type="checkbox" value="" id="checkbox">

  Autorización de uso de imagen sobre fotografías (Autorizo a la UAE Junta Central de Contadores a la utilización de derechos de imagen sobre fotografías o procedimientos análogos a la fotografía o producciones audiovisuales, así como los derechos patrimoniales de autor (reproducción, comunicación, transformación y/o distribución) y derechos conexos, sin limitación geográfica o territorial alguna. Esta información será utilizada en ediciones electrónicas, digitales y en la red de internet.). Este campo es obligatorio. 
  </li>
  <li>
  <input class="form-check-input" type="checkbox" value="" id="checkbox">

  Acepto notificaciones por correo electrónico (Autorizo a la UAE – JUNTA CENTRAL DE CONTADORES a realizar la notificación legal de cualquier documento o acto administrativo a través del correo electrónico indicado en este formato de conformidad con lo estipulado en el Artículo 56 de la Ley 1437 de 2011). 
  </li>
  <li>
  <input class="form-check-input" type="checkbox" value="" id="checkbox">

  Acepto bajo la gravedad de juramento que no tengo inhabilidades e incompatibilidades (Bajo la gravedad de juramento manifiesto que no me encuentro inmerso en causales de inhabilidades, incompatibilidades, impedimentos o conflictos de interés, conforme a lo previsto en el artículo 7 del Decreto 1955 de 2010). Es obligatorio seleccionar
  </li>
  <hr class="hrCandidate">
  <div class="form-check">
  <!-- <input class="form-check-input" type="checkbox" value="" id="checkbox"> -->
  <!-- <label class="form-check-label" for="flexCheckIndeterminate">
    Acepto
  </label> -->
</div>
</ul>

      <br>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <br>
        <button type="button" class="btn btn-raised  btn-success" data-dismiss="modal" onclick="carge()" >
        Siguiente
        </button>
      </div>      
    </div>
    </div>
  </div>
</div>      


    
 
<div class="col-xs-12 col-md-12 documentos" id="documentos" style="display: none">
<div  class='row'>
<div class="col-xs-12 col-md-12">

    <p>Documentos requeridos para continuar la Inscripción</p>
          <hr>
          </div>

    <div class="col-sm-12">
      <!-- <label for="usr">Imagen de perfil</label> -->
      <p>Fotografia</p>
      <input type="file" class="form-control-file" name="img_candidate" accept="image/*" required>
      <br>
      <div class="alert alert-warning alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Fotografia </strong>Debe ser en formato JPG correspondiente a una 
    fotografía reciente 3x4 cms, 342x387 pixeles, con Resolución a 300 dpi, 
    tomadas de frente.
  </div>
    </div>

    <div class="col-sm-12">
    <!-- <label for="usr">Cedula de ciudadania</label> -->
    <p>Cedula de ciudadania</p>
    <input type="file" class="form-control-file" name="doc_candidate" accept="application/pdf" required>
  
    <hr>
    <div class="alert alert-warning alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Cedula de ciudadania </strong>Debe ser un documento en formato PDF ampliado al 150% 
    de la cédula de ciudadanía o cedula de extranjería del solicitante.
  </div> 
  </div>
    <div class="col-sm-12">
    <!-- <label for="usr">Certificados de antecedentes</label> -->
    <p>Certificados de antecedentes</p>
    <input type="file" class="form-control-file" name="certificado_candidate" accept="application/pdf" required>
    <hr>
    <div class="alert alert-warning alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Certificados de antecedentes!</strong>
    Debe ser un documento en formato PDF, en un solo archivo, del certificado vigente de
    antecedentes expedidos por la Contraloría General de la República, 
    Procuraduría General de la Nación y Policía Nacional.  </div>  
  </div>
    <div class="col-sm-12">
    <!-- <label for="usr">carta de aceptación</label> -->
    <p>carta de aceptación</p>
    <input type="file" class="form-control-file" name="carta_candidate" accept="application/pdf" required>
    <hr>
    <div class="alert alert-warning alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Carta de aceptación</strong>
    Debe ser un documento en formato PDF, en un solo archivo, de la carta de aceptación expresa de la postulación;
    este documento solo se requiere para candidatos de instituciones 
    de educación superior.  </div>  
  </div>

    </div>
    <br>
<button  class="btn btn-raised btn-danger">
        <i class="fa fa-check" aria-hidden="true"></i>
      Finalizar Inscripción</button>  
</div>


</div>



</form>

<?php

}else {
  if(!empty($_GET["ok"])){
  $ok = $_GET["ok"];
  echo <<<END
  <script type="text/javascript">
  function sayHi() {
    Swal.fire({
      icon: 'success',
      title: 'Registro exitoso',
      timer: 15500,
      footer: '<a href="../pdf.php?ok=$ok" target="_blank">Descargar Certificado de Registro</a>'
    })

  }
   setTimeout(sayHi, 100);
  </script>
  
  END;

  }
  ?>



<div class="titulo">
<h3>
  Inscripciones para el proceso de elecciones de miembros del tribunal  
  Disciplinario de la Junta Central de Contadores 
  <br>
  <strong>2022 - 2025</strong> 
</h3>
<hr>
</div>

<div class="alert alert-danger alert-dismissible">
  
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Acualización de datos!</strong> Recuerde realizar la actualización de datos antes de continuar.
    <a href="https://www.google.com/" target="_blank"">Debé dar clic <strong>Aquí</strong></a>
  </div>

  <div class="row">
  <div class="col-sm-6">
  <form action="" method="post"  class="was-validated"enctype="multipart/form-data">
    <div class="col-sm-12 inscriction">
      <input type="hidden" id="institution_id" name="institution_id" value="Para candidatos a representante de los contadores">
      <button type="submit" class="btn inscriction-btn" id="inscriction-btn"> 
      Para candidatos a representante de los contadores
      </button>
    </div>
  </form>

  </div>
  <div class="col-sm-6">
  <form action="" method="post" enctype="multipart/form-data">
      <div class="col-sm-12 inscriction">
          <input type="hidden" id="institution_id" name="institution_id" value="Para candidatos a representante de Instituciones Educativas">

          <button type="submit" class=" btn inscriction-btn" id="inscriction-btn">
          Para candidatos a representante de Instituciones Educativas
 
        </button>

      </div>

  </form>


</div>
  </div>

 
  

  <?php

}



?>
<!-- models of the form to register a new votante -->
<script>


function validacion() {
  alert('validación')
}
function carge(){

  if (document.getElementById('checkbox').checked)
{
  // alert('ahora vamos a cargar los documentos');
  Swal.fire('Acepto los terminos y condiciones, ahora se deben cargar los documentos para finalizar el proceso')

  document.getElementById('documentos').style.display = 'block';
  document.getElementById('btn-siguiente').style.display = 'none';
  // document.getElementById('nombre1Inscrito').disabled = true;
  // document.getElementById('nombre2Inscrito').disabled = true;
  // document.getElementById('apellido1Inscrito').disabled = true;
  // document.getElementById('apellido2Inscrito').disabled = true;
  // document.getElementById('nacimientoInscrito').disabled = true;
  // document.getElementById('fechaInscrito').disabled = true;
  // document.getElementById('GeneroInscrito').disabled = true;

  // document.getElementById('documentInscrito').disabled = true;
  // document.getElementById('typedoc').disabled = true;
  // document.getElementById('lugarInscrito').disabled = true;
  // document.getElementById('CodigoInscrito').disabled = true;

  // document.getElementById('documentfechaInscrito').disabled = true;
  // document.getElementById('documentfechaInscrito').disabled = true;
  // document.getElementById('tarjetaInscrito').disabled = true;
  // document.getElementById('correoInscrito').disabled = true;
  // document.getElementById('telefonoCInscrito').disabled = true;
  // document.getElementById('telefonoFInscrito').disabled = true;
  // document.getElementById('ciudadInscrito').disabled = true;

  // document.getElementById('AT_Inscrito').disabled = true;
  
}else{
  Swal.fire(
    {
      icon: 'error',
      title: 'Debe aceptar terminos para continuar'
      // text: 'Something went wrong!',
    }
  )


}

}


function registro(){

  Swal.fire(
  'Registro Exitoso!',
  '',
  'success'
)
}

</script>