
<!-- models of the form to register a new votante -->

          <form action="../../controllers/register/register_students.php?id=<?php echo "".$id."";?>" method="post" class="">
          <div  class='row'>
                <div class="col-xs-12 col-md-6">
                    <label for="usr">Nombre del votante</label>
                    <input name="nameStudent" type="text" class="form-control" id="nameStudent" required>
                </div>

                <div class="col-xs-12 col-md-6">
                  <label for="usr">Apellido de votante</label>
                  <input name="lastStudent" type="text" class="form-control" id="lastStudent" required>
                </div>

                <div class="col-xs-12 col-md-6">
                    <label for="pwd">Documento</label>
                    <input name="document" type="text" class="form-control" id="document" required>
                </div>

                <div class="col-xs-12 col-md-6">            
                    <label for="pwd">Grupo</label>
                    <input name="grade" type="text" class="form-control" id="grade" required>
                </div>

                
                <div class="col-sm-6">
                    <label for="usr">Coeficiente Electoral</label>
                    <input class='form-control'  type='number'  min='0' max='100' step='any' name='electoral_quotient' required>
                </div>  

                
                <div class="col-sm-12">
                  <hr>
                </div> 
             
                <div class="col-xs-12 col-md-6">
                    <label for="usr">Correo o nombre de Usuario</label>
                    <input name="emailStudent" type="text" class="form-control" id="emailStudent" required>
                </div>

  
                <div class="col-xs-12 col-md-6">
                    <label for="pwd">contraseña asignada</label>
                  <input name="passwordStudent" type="password" class="form-control" id="passwordStudent" required>            
                </div>



            </div>
 
            <div class="row">


                <!-- <div class="col-sm-6">
                <label for="exampleFormControlSelect1">Activar votante</label>
                  <select name="special" id="special" class="form-control" id="exampleFormControlSelect1">
                      <option value='A' selected>Activo</option>
                      <option value = 'I' selected>Inactivo</option>
                  </select>
                </div>   -->
                </div>
                
                <div class="col-sm-12">
                  <hr>
                </div> 
              
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Importante!</strong> Ingresar coeficiente asignado al votante.
                </div>
           

                
            <br>
            <button type="submit" class="btn btn-raised btn-danger">
            <i class="fa fa-check" aria-hidden="true"></i>
               Registrar</button>  
          <br>
        
        </form>
        

      <a type="submit" id="behind" href="../../view/students_inst.php?id=<?php echo "".$id."";?>" class="btn btn-raised btn-primary"><i class="fa fa-arrow-left"></i>  Atrás</a>  