<!-- models of the form to register a new assembly -->     
<?php

$admin =  $_GET["id"];
if(isset($_GET["inst"])){
$institution_id =  $_GET["inst"];

  ?>
  <form action="../../controllers/register/register_assembly.php?id=<?php echo "".$admin."";?>&inst=<?php echo "".$institution_id."";?>" method="post" enctype="multipart/form-data">
  <div  class='row'>

        <div class="col-sm-12">
              <label for="usr">Asignarle un nombre a la asamblea</label>
              <input name="nameassembly" type="text" class="form-control" id="nameStudent" required>
              <hr>
              <label for="usr">Seleccione una imagen representativa</label>
              <br>
              <input type="file" class="form-control-file" name="img_assembly" accept="image/*">
              <hr>   
        </div>

        <div class="col-sm-6">
                <br>
                <label for="usr">Fecha de Inicio</label>
                <input name="startdate" type="date" class="form-control">
                <br>
                <label for="usr">Hora de inicio</label>
               <input name="startime" type="time" class="form-control">
        </div>
        
        <div class="col-sm-6">
            <br>
            <label for="usr">Fecha Final</label>
            <input name="enddate" type="date" class="form-control">
            <br>
            <label for="usr">Hora final</label>
            <input name="endtime" type="time" class="form-control">
        </div>

      <div class="col-sm-12">
          <hr>
          <label for="usr">Descripción</label>
          <input name="descriptionassemly" type="`text`" class="form-control">
          <hr>

          <input name="stade" type="hidden" class="form-control" id="stade" value="I">
          <input name="special" type="hidden" class="form-control" id="special" value="I">
          <input name="inst" type="hidden"class="form-control" id="inst" value='<?php echo "".$_GET["inst"]."";?>'>


          <br>
          <button type="submit" class="btn btn-raised btn-danger">
          <i class="fa fa-check" aria-hidden="true"></i>
           Registrar</button>  

      </div>


  </div>
  </form>

  <a type="submit" id="behind" href="../../view/assembly_inst.php?id=<?php echo "".$id."";?>" class="btn btn-raised btn-primary"><i class="fa fa-arrow-left"></i>  Atrás</a>  



<?php
}elseif(isset($_POST["inst"])){
$institution_id =  $_POST["inst"];

?>

<form action="../../controllers/register/register_assembly.php?id=<?php echo "".$admin."";?>&inst=<?php echo "".$institution_id."";?>" method="post" enctype="multipart/form-data">
  <div  class='row'>
        <div class="col-sm-12">
              <label for="usr">Asignarle un nombre a la asamblea</label>
              <input name="nameassembly" type="text" class="form-control" id="nameStudent" required>
              <hr>
              <label for="usr">Seleccione una imagen representativa</label>
              <br>
              <input type="file" class="form-control-file" name="img_assembly" accept="image/*">
              <hr>
  </div>

        <div class="col-sm-6">
                <br>
                <label for="usr">Fecha de Inicio</label>
                <input name="startdate" type="date" class="form-control">
                <br>
                <label for="usr">Hora de inicio</label>
                <input name="startime" type="time" class="form-control">
        </div>
        
          <div class="col-sm-6">
            <br>
            <label for="usr">Fecha Final</label>
            <input name="enddate" type="date" class="form-control">
            <br>
            <label for="usr">Hora final</label>
            <input name="endtime" type="time" class="form-control">
          </div>

      <div class="col-sm-12">
          <hr>
          <label for="usr">Descripción</label>
          <input name="descriptionassemly" type="`text`" class="form-control">
          <hr>

          <input name="stade" type="hidden" class="form-control" id="stade" value="I">
          <input name="special" type="hidden" class="form-control" id="special" value="I">
          <input name="inst" type="hidden"class="form-control" id="inst" value='<?php echo "".$_POST["inst"]."";?>'>
<!-- 
          <label for="exampleFormControlSelect1">Estado</label>
          <select name="stade" id="stade" class="form-control" id="exampleFormControlSelect1">
            <option value = 'I' selected>Inactivo</option>
            <option value='A' selected>Activo</option>
          </select>
          <hr>

          <label for="exampleFormControlSelect1">Asamblea especial (si activa esta casilla los candidatos podran inscribirse independientemente)</label>
          <select name="special" id="special" class="form-control" id="exampleFormControlSelect1">
            <option value='A' selected>Activo</option>
            <option value = 'I' selected>Inactivo</option>
          </select> -->
          <br>
          <button type="submit" class="btn btn-raised btn-danger">
          <i class="fa fa-check" aria-hidden="true"></i>
           Confirmar Registro</button>  

        </div>


          <br>
  </div>
  </form>

  <a type="submit" id="behind" href="../../view/assembly_inst.php?id=<?php echo "".$id."";?>" class="btn btn-raised btn-primary"><i class="fa fa-arrow-left"></i>  Atrás</a>  



<?php

}else
   
if(empty($_POST["inst"])){


        // ESTA FUNCION ES CREADA PARA VERIFICAR QUE LA INSTITUCION YA NO TENGA ASIGNADA UNA ASAMBLE

        function validate($admin,$conn){   
            
          $controls = "SELECT * FROM `assemblies` 
          WHERE admin_id = '$admin'
          ";

          $control = $conn->query($controls);
            
          // var_dump($control->fetch_assoc()['institution_id']);
          if ($control->num_rows > 0) {
              return $control->fetch_assoc()['institution_id'];
          }
          return null;
        }

              

   

  echo '<form action="../../view/admin/r_assembly.php?id='.$admin.'" method="post" enctype="multipart/form-data">';
  echo '<div  class="row">';

            echo '<div class="col-sm-12">';

            echo '<label for="pwd">Seleccionar Institución</label>';
          // <!-- mostrar opciones de asambleas registradas para el registro del candidato -->

            echo "<select class='form-control' id='inst' name='inst' >";
                // echo "<option value='0' selected>" ."Elija una asamblea". "</option>";
                $sql = "SELECT * FROM institutions WHERE admin_id  = '$admin'";
                      
                $result = $conn->query($sql);
                while ($row = $result->fetch_assoc()) {

                $institution_id= $row['institution_id'];

                  echo '<option value="'.$row['institution_id'].'">'.$row['name'].'</option>';         
                      
                }

            echo "</select>";
            echo '<br>';

            
            echo '<button type="submit" class="btn btn-raised btn-danger">
            <i class="fa fa-check" aria-hidden="true"></i>
            Seleccionar</button>';

            
    
            
          echo '</div>';

          echo '</form>';
          
          echo '</div>';

          
                
          ?>
          <a type="submit" id="behind" href="../../view/assembly_inst.php?id=<?php echo "".$id."";?>" class="btn btn-raised btn-primary"><i class="fa fa-arrow-left"></i>  Atrás</a>  
         
          <?php
}



?>






  

