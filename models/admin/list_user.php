    
<?php
$admin_id =  $_GET["id"];
$sql = "SELECT * FROM institutions WHERE admin_id = '$admin_id'";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
  $ia = 0;
  echo' <table id="example1" class="table table-bordered table-striped">';
  echo' <thead>';
  echo'  <tr>';
  echo'  <th scope="col">Id</th>';
  echo'  <th scope="col">Nombre</th>';
  echo'  <th scope="col">Usuario de Ingreso</th>';
  echo'  <th scope="col">Estado</th>';
  echo'  <th scope="col">Descripción</th>';
  echo'  <th scope="col">Editar</th>';
  echo'  <th scope="col">Eliminar</th>';
  echo'  </tr>';
  echo'</thead>';
  echo' <tbody>';
  while ($row = $result->fetch_assoc()) {
    $institution_id = $row['institution_id'];
    echo'<tr>';
    echo '<td>' . $row['institution_id'] . '</td>';
    echo '<td>' . $row['name'] . '</td>';
    echo '<td>' . $row['email'] . '</td>';   
    echo '<td>' . $row['state'] . '</td>'; 
    echo '<td>' . $row['description'] . '</td>';   
    echo '<td>
    <a href="admin/edit_user.php?id='.$row["institution_id"].'&inst='.$admin_id.'">
    <i class="fas fa-edit"></i></a>
    </td>';
    echo '<td>
    <a onclick="javascript: return confirm(\'Desea eliminar el registro actual?\')" href="../controllers/admin/remove_user.php?inst='.$institution_id.'&id='.$admin_id.'">
    <i class="fa fa-trash" aria-hidden="true"></i></a>
    </td>';  
    echo'  </tr>';    
    }

    echo'</tbody>';
    echo' </table>';
  }else {
    echo 'Aún no existen Instituciones registradas';
    echo '<hr>';
    echo '<a type="submit" href="../view/new_user.php?id='.$admin_id.'" class="btn btn-raised btn-danger">
    <i class="fa fa-check" aria-hidden="true"></i>
    Registrar Institución</a>';
  }

?>                


  