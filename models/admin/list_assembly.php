<?php

$admin_id =  $_GET["id"];
$sql = "SELECT 
assemblies.assembly_id,
assemblies.institution_id,
institutions.institution_id,
institutions.name AS name_institutions,
assemblies.name,
assemblies.start_date,
assemblies.end_date,
assemblies.img_assembly,
assemblies.type,
assemblies.description
FROM assemblies 
INNER JOIN institutions 
ON assemblies.admin_id = '$admin_id'
AND assemblies.institution_id = institutions.institution_id";

$result = $conn->query($sql);
if ($result->num_rows > 0) {

  $ia = 0;
  echo' <table id="example1" class="table table-bordered table-striped">';
  echo' <thead>';
  echo'  <tr>';
  // echo'  <th scope="col">Id</th>';
  echo'  <th scope="col"></th>';
  echo'  <th scope="col">Nombre</th>';
  echo'  <th scope="col">Fecha de Inicio</th>';
  echo'  <th scope="col">Fecha Final</th>';
  echo'  <th scope="col">Institución</th>';
  echo'  <th scope="col">Categorias</th>';
  echo'  <th scope="col">Editar</th>';
  // echo'  <th scope="col">Eliminar</th>';
  // echo'  <th scope="col">Contraseña</th>';
  echo'  </tr>';
  echo'</thead>';
  echo' <tbody>';
  while ($row = $result->fetch_assoc()) {
    echo'<tr>';
    // echo '<td>' . $row['assembly_id'] . '</td>';
    $type =$row['type'];
    $assembly_id = $row["assembly_id"];
    $institution_id = $row["institution_id"];
    echo '<td>','<img src="data:'.$type.';base64,'.base64_encode($row['img_assembly']).'" alt="" width="auto" height="50">','</td>';
    echo '<td>' . $row['name'] . '</td>';
    echo '<td>' . $row['start_date'] . '</td>';   
    echo '<td>' . $row['end_date'] . '</td>';   
    // echo '<td>' . $row['description'] . '</td>'; 
    echo '<td>' . $row['name_institutions'] . '</td>';   
  

    // echo '<td>' . base64_encode($row['img_assembly']) . '</td>';   

    echo '<td>
    <a href="admin/category.php?inst='.$institution_id.'&assembly='.$assembly_id.'&id='.$admin_id.'">
    <i class="fas fa-poll-h"></i>
    </a>
    </td>';
    
    echo '<td>
    <a href="admin/edit_assembly.php?inst='.$institution_id.'&assembly='.$assembly_id.'&id='.$admin_id.'">
    <i class="fas fa-edit"></i></a>
    </td>';

    // echo '<td>
    // <a onclick="javascript: return confirm(\'Desea eliminar el registro actual?\')" href="../controllers/admin/remove_assembly.php?inst='.$institution_id.'&assembly='.$assembly_id.'&id='.$admin_id.'">
    // <i class="fa fa-trash" aria-hidden="true"></i></a>
    // </td>';          
    }

    echo'</tbody>';
    echo' </table>';

  }else {
    echo 'Aún no existen Asambleas registradas';
    echo '<hr>';

    echo '<a type="submit" href="../view/new_user.php?id='.$admin_id.'" class="btn btn-raised btn-danger">
    <i class="fa fa-check" aria-hidden="true"></i>
    Registrar Institución</a>';

    
  }

    
     
?>                     