<?php
$institution_id =  $_GET["inst"];
$assembly_id =  $_GET["assembly"];
$admin_id =  $_GET["id"];


  //MUMERO DE ESTUDIANTES HABILITADOS PARA VOTAR

  $sql="SELECT 
  COUNT(students.students_id) AS total_students,
  SUM(students.electoral_quotient) AS quotient_students,
    students.name 
   FROM students 
   WHERE students.institution_id=$institution_id
   ";


$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
$totalstudent = $row['total_students'];
$quotient_students = $row['quotient_students']; 
} 

$sql="SELECT 
COUNT(controls.students_id) AS total,
SUM(controls.electoral_quotient) AS controls_quotient
FROM controls 
WHERE controls.institution_id=$institution_id ";


$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
$totalvotos = $row['total'];
$controls_quotient = $row['controls_quotient'];

}



//se realiza la consulta para mostrar el numero de votos por candidato
$sql="SELECT votes.assembly_id,
SUM(votes.total_votes) AS TOTAL, 
assemblies.name AS assemblies, 
votes.candidate_id AS candidate_id,
COUNT(votes.candidate_id) AS votos,
candidates.name AS candidate,
candidates.last AS candidatelast,
candidates.category AS category,
assemblies.end_date AS end_date,
assemblies.start_date AS start_date,
candidates.type,
candidates.img_candidate
FROM votes
INNER JOIN candidates 
ON votes.candidate_id=candidates.candidate_id 
INNER JOIN assemblies 
ON votes.assembly_id=assemblies.assembly_id 
AND votes.institution_id = $institution_id
GROUP BY assemblies, candidate";

$result = $conn->query($sql);
  
  $i = 0;
  $voto = 0;
  echo' <table id="example1" class="table table-bordered table-striped">';
  echo' <thead>';
  echo'  <tr>';
  // echo'  <th scope="col">Id</th>';
  echo'  <th scope="col"></th>';
  echo'  <th scope="col">Candidato</th>';
  echo'  <th scope="col">Categoria</th>';
  echo'  <th scope="col">Asamblea</th>';
  echo'  <th scope="col">Votos</th>';
  // echo'  <th scope="col">Total</th>';
  // echo'  <th scope="col">Borrar datos</th>';

  echo'  </tr>';
  echo'</thead>';
  echo' <tbody>';
  while ($row = $result->fetch_assoc()) {
    $i++;
    $name_assembly = $row['assemblies'];
    $last_candidate = $row['candidatelast'];
    $name_candidate = $row['candidate'] ;
    $category = $row['category'] ;
    $start_date = $row['start_date'];
    $end_date = $row['end_date'] ;
    $cuoeficiente = $row['TOTAL']; 
    $votos = $row['votos']; 
    $total = round((($cuoeficiente*100)/$quotient_students),1);



    $type = $row['type'];

    echo'<tr>';
    // echo '<td>' . $i. '</td>';
    echo '<td>','<img src="data:'.$type.';base64,'.base64_encode($row['img_candidate']).'" alt="" width="40" height="40">','</td>';
    echo '<td>' . $name_candidate . ' ' .$last_candidate.'</td>';
    echo '<td>' . $category. '</td>';
    echo '<td>','<a href="#" data-toggle="popover" title="Detalle de Asamblea" data-content=" Asamble activa del '.$start_date.'  hasta  '.$end_date.'">' . $name_assembly . '</a>' ,'</td>';
    echo '<td>' . $votos.' ','</td>';  
    // echo '<td>' . $total.' %','</td>';  
    // echo '<td>
    // <a onclick="javascript: return confirm(\'Desea eliminar el registro del candidato?\')" href="../controllers/admin/remove_votecandidate.php?control='.$row["candidate_id"].'&assembly='.$assembly_id.'&inst='.$institution_id.'&id='.$admin_id.'">
    // <i class="fa fa-trash" aria-hidden="true"></i></a>
    // </td>'; 
    
      
    }
    echo'</tbody>';
    echo' </table>';  



    echo '<div>
    <a class="btn btn-danger" onclick="javascript: return confirm(\'Seguro que desea eliminar todos los resultados?\')" href="../controllers/admin/remove_voter.php?assembly='.$assembly_id.'&inst='.$institution_id.'&id='.$admin_id.'">
    <i class="fa fa-trash" aria-hidden="true"></i> Limpiar resultados de las elecciones</a>
    </div>'; 

    echo '<hr>';

    
    echo '<a type="submit" class="btn btn-outline-dark" data-mdb-ripple-color="dark" href="control_admin.php?id='.$admin_id.'">
    <i class="fa fa-arrow-left" aria-hidden="true"></i>
    Atrás
    </a>';

    



    
     
?>  



