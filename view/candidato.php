<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php require 'links/bootstrap.php'?>
    <link rel="stylesheet" href="css/formulario.css">
    <link rel="stylesheet" href="css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Incripción como Candidato</title>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />

</head>
<body>

<header class="header">
<nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
  <!-- Brand -->
  <a class="navbar-brand" href="#"><i><img src="img/escudo.png" alt="" alt="" width="40" height="40"></i></a>  
    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse collapse"  id="collapsibleNavbar">
        <ul class="navbar-nav ml-auto">
            <!-- <li class="nav-item">
            <a> <?php echo $name_institutions?> </a>
        </li> -->
    </div>
  <!-- Navbar links -->   
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2"  id="collapsibleNavbar">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <a class="nav-link" href="" ><i class="fa fa-fw fa-user"></i> Salir</a>
            </li>
        
        </ul>
    </div>
</nav>
  
</header>        


    <div class="container inscription">
    <div  class='row'>
      
        <div class="col-xs-12 col-md-3">

        <div class="lateral">
            <img class="imgCandidate" src="img/jcc-logo.png"  width="260" height="auto">
            <hr class="hrCandidate">
    <p>Normatividad aplicable en el proceso de elección</p>

            <ul>
    <li>
      <a href="">Resolución 000-1418 de 2021</a>
    </li>
    <li>
      <a href="">Resolución 860 de 2020</a>
    </li>
    <li>
      <a href="">Ley 43 de 1990</a>
    </li>
    <li>
      <a href="">Ley 734 de 2002</a>
    </li>
    <li>
      <a href="">Decreto 1955 de 2010</a>
    </li>
  </ul>           
   <hr class="hrCandidate">

        </div>
        </div>

    <div class="col-xs-12 col-md-9">
            <div class="card">
            <div class="card-header">
                Formulario de incripción de candidato
            </div>

            <div class="card-body">
            <?php require '../models/candidate.php'?>
    </div>
</div>



</div>

</div>

</body>
</html>

