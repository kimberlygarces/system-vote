<?php require 'template_user.php'?>

<div class="container-fluid">

      <div class="card">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-exclamation"></i> Quórum vs votantes habilitados</h3>
              </div>
                <div class="card-body">
                <?php require '../models/institution/q_vs_v.php' ?>
                </div>
      </div>
    </div>
  <div class="card">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-exclamation"></i> Lista Quórum</h3>
              </div>
                <div class="card-body">
                <?php require '../models/institution/quorum.php' ?>
        </div>
      </div>
    </div>
  </div>
</section>

<script src="plugins/chart.js/Chart.min.js"></script>

<?php require 'template_footer.php'?>

<script type="text/javascript">
  $(function () { 

    var donutChartCanvas = $('#pieChart').get(0).getContext('2d')

    $totalvotes='<?php echo $totalvotes?>';
    $total_quorun = '<?php echo $total_quorun?>';

    var donutData = {
      labels: [
          'Votantes habilitados sin registro de quórum',
          'Quórum registrado',


      ],
      datasets: [
        {
          data: [$totalvotes,$total_quorun],
          backgroundColor : ['#f56954', '#00a65a'],
        }
      ]
    }

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieData        = donutData;
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })

  })

</script>









