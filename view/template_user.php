<?php
session_start();
$user=$_SESSION['institucion'];
if (!isset($_SESSION['institucion'])){
  header("location:../index.php");
}
require '../controllers/conection.php';
require '../controllers/data_institution.php';
$id_grupo=$_GET['id'];
// echo $id;
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Sistema de votaciones</title>
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <link rel="shortcut icon" href="img/votacion.png" />

</head>

<style>

#sidebar{
 
  position: fixed;
    font-family: "Consolas", monospace;
    font-size: 11pt;

}

/* #card-category{

    width: auto;

} */

#content-wrapper{
  padding-top: 54px;
  text-align: center;

}

.card-header{
    text-align: center;
    font-family: "Consolas", monospace;
    font-size: 16pt;
    background: -moz-linear-gradient(270deg, rgb(2, 22, 199) 0%, rgb(86, 94, 206) 100%);
    background: -webkit-linear-gradient(270deg, rgba(0, 0, 0, 0.349) 0%, rgba(89, 78, 193, 0.336) 100%);
    border: none;
    border-radius: 3;    
}

.card{
  margin: 2%;
  text-align: center;
  font-family: "Consolas", monospace;

}

</style>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light fixed-top">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="../index.php" class="nav-link">Home</a>
      </li>
      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
    
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
      <a class="nav-link" href="../controllers/login/logout.php?id=<?php echo "".$id_institutions.""?>"> <i class="fa fa-fw fa-user"></i>Salir</a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside id="sidebar" class="main-sidebar sidebar-dark-primary">
    <!-- Brand Logo -->
    <a href="../index.php" class="brand-link">
      <!-- <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
      <span class="brand-text font-weight-light">Sistema de votaciones</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <!-- <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div> -->
        <div class="info">
          <?php echo '<a class="d-block" href="#">'."  ".$name_institutions. '</a>';?>
        </div>
      </div>
    

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <!-- menu para listar usuarios -->
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- <li class="nav-item menu-open">
            <a href="#" class="nav-link active">
            <i class="fas fa-boxes"></i>
               <p>
                Mis asambleas
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li>
                <a class="nav-link" href="institution/r_assembly.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fas fa-boxes"></i> Nueva Asamblea</a>
                </li>
                <li>
                <a class="nav-link" href="assembly_inst.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fa fa-list-ul">  </i> Listar Asambleas</a>
                </li>
            </ul> -->

            <li>
              <a class="nav-link"href="home_inst.php?id=<?php echo "".$id_grupo."";?>" class="download">
                <i class="fas fa-home"></i>
                Principal</a></li>
                </a>
            </li>
            <li>
              <a class="nav-link"href="link_conection.php?id=<?php echo "".$id_grupo."";?>" class="download">
              <i class="fa fa-link" aria-hidden="true"></i>
                Enlace de conexión</a></li>
                </a>
            </li>

          <li class="nav-item">
            <a href="#" class="nav-link active">
            <i class="fas fa-portrait"></i>
               <p>
                Mis candidatos
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li>
                <a class="nav-link" href="institution/r_candidate.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fas fa-portrait"></i> Nuevo candidato</a>
                </li>
                <li>
                <a class="nav-link" href="candidate_inst.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fa fa-list-ul"></i> Lista de candidatos</a>
                </li>
            </ul>

            <li class="nav-item">
            <a href="#" class="nav-link active">
            <i class="fas fa-user-check"></i>
               <p>
                Usuario auditor
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li>
                <a class="nav-link" href="institution/auditor.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fas fa-user-check"></i> Inscribir auditor</a>
                </li>
            </ul>

            <li class="nav-item ">
            <a href="#" class="nav-link active">
            <i class="fas fa-user-friends"></i>
              <p>
                Votantes
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li>
                <a class="nav-link" href="institution/r_votante.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fas fa-user-circle"></i> Nuevo votante</a></li>
                <li>
                <a class="nav-link" href="students_inst.php?id=<?php echo "".$id_grupo."";?>">
                <!-- <a class="nav-link" href="" onclick="voter()"> -->
                <i class="fa fa-list-ul"></i> Lista de votantes</a></li>
            </ul>
        
        <!-- opciones de resultados de la assamblea -->

        <li class="nav-item menu-open">
            <a href="#" class="nav-link">
            <i class="fas fa-chart-pie"></i>
              <p>
                Resultados
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
              <ul class="nav nav-treeview">
           

                  
                  <li>
                  <a class="nav-link" href="quorum_inst.php?id=<?php echo "".$id_grupo."";?>" class="download">
                  <i class="fa fa-list-ul"> </i>
                  Quórum registrado</a></li>
                  </a>
                  </li>

                  <li>
                  <a class="nav-link" href="control_inst.php?id=<?php echo "".$id_grupo."";?>" class="download">
                  <i class="fas fa-vote-yea"></i>
                  Votos registrados</a></li>
                  </a>
                  </li>

                  <li>
                  <a class="nav-link" href="no_voter.php?id=<?php echo "".$id_grupo."";?>" class="download">
                  <i class="fas fa-times"></i>
                  No votantes</a></li>
                  </a>
                  </li>
              </ul>
              <ul class="nav nav-treeview">
                  <li>
                    <a class="nav-link"href="control.php?id=<?php echo "".$id_grupo."";?>" class="download">
                    <i class="fas fa-chart-pie"></i>
                    Resultados</a></li>
                    </a>
                    </li>
              </ul>
              <!-- <li class="nav-item">
            <a href="#" class="nav-link active">
            <i class="fas fa-cog"></i>
               <p>
                Personalizar
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <li>
                <a class="nav-link" href="institution/auditor.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fas fa-user-check"></i> Mis datos</a>
                </li>
            </ul> -->

           
      </nav>

      
    </div>
  </aside>
  <section id="content-wrapper" class="content-wrapper">


      


