<?php
session_start();
$user=$_SESSION['admin'];

if (!isset($_SESSION['admin'])) {
    header("location:../index.html");
}

require '../controllers/conection.php';
require '../controllers/data.php';

$id=$_GET['id'];
// echo $id;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistema de votaciones</title>
    <link rel="stylesheet" href="css/style.css">

    <?php require 'links/bootstrap.php'?>
</head>
<body>
<div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3>Sistema de Votaciones</h3>
        </div>

        <ul class="list-unstyled components">
            <p>Bienvenido</p>
        <li class="active" id="active">
        <!-- <i class="fa fa-home"></i>  -->
        <a id="p" href="#">Mi Perfil </a>
        <a id="p" href="#homeSubmenuA" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Usuarios</a>
            <ul class="collapse list-unstyled" id="homeSubmenuA">
                <li>
                <a class="nav-link" href="../models/register/register_institution.php?id=<?php echo "".$id."";?>">
                <i class="fas fa-user-plus"></i> Registrar nuevo </a>
                </li>
                <li>
                <a class="nav-link" href="grup_admin.php?id=<?php echo "".$id."";?>">
                <i class="fa fa-list-ul"></i> Listar mis usuarios</a>
                </li>
            </ul>
           
            <!-- <a id="p" href="#homeSubmenuC" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Mis candidatos</a>
            <ul class="collapse list-unstyled" id="homeSubmenuC">
                <li>
                <a class="nav-link" href="../models/register/register_candidate.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fas fa-user-plus"></i> Nuevo candidato</a>
                </li>
                <li>
                <a class="nav-link" href="candidate_inst.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fa fa-list-ul"></i> Lista de candidatos</a>
                </li>

            </ul> -->
            <!-- <a id="p" href="#homeSubmenuV" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Votantes</a>
            <ul class="collapse list-unstyled" id="homeSubmenuV">
                <li>
                <a class="nav-link" href="../models/register/register_students.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fas fa-user-plus"></i> Nuevo votante</a></li>
                <li>
                <a class="nav-link" href="students_inst.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fa fa-list-ul"></i> Lista de votantes</a></li>
            </ul> -->


            <ul class="list-unstyled CTAs">
                <li>
                <a href="control.php?id=<?php echo "".$id_grupo."";?>" class="download">
                <i class="fa fa-key"></i> Control del sistema</a></li>
                </a>
                </li>
             </ul>
          
            <ul class="list-unstyled CTAs">
                <li>
                <a href="resultassembly.php?id=<?php echo "".$id_grupo."";?>" class="download">
                <i class="fa fa-fw fa-user"></i>Resultados Generales</a></li>
                </a>
                </li>
             </ul>

    </nav>

    <!-- Page Content Holder -->
    <div id="content">
        <nav class="navbar navbar-expand-lg" style="background-color: #af1919;">

            <div class="container-fluid">
                <button type="button" id="sidebarCollapse" class="navbar-btn">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav pull-xs-right">
                    <li class="nav-item">
                        <a class="nav-link">
                        <i><img src="img/user.png" alt="" width="70" height="60">
                        <!-- <?php echo '<a>'."  ".$nameAdmin. '</a>';?> -->
                        </i> </a>
                    </li>
                        <li class="nav-item" id="name">
                             <?php echo '<a class="nav-link" href="#">'."  ".$nameAdmin. '</a>';?>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="../controllers/login/logout.php"> <i class="fa fa-fw fa-user"></i>Salir</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </head>

<div class="container">
    
<!-- <div>
    <h1> user <?php echo "".$user."";?> </h1>
    <h1>voting system</h1>
    <h2>Vista Solo para administrador del sitio</h2>
</div>

<a class="nav-link" href="../models/register/register_institution.php?id=<?php echo "".$id."";?>"><i class="fa fa-fw fa-user"></i>Registrar Institución</a> -->


</div>


    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $(this).toggleClass('active');
            });
    }); 
   </script>

<script>
        $(document).ready(function(){
        $('[data-toggle="popover"]').popover();   
        });
</script>


    
</body>
</html>