<?php
session_start();
$user=$_SESSION['admin'];
if (!isset($_SESSION['admin'])) {
    header("location:../index.html");
}
require '../../controllers/conection.php';
require '../../controllers/data.php';
$id=$_GET['id'];
// echo $id;
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>SV <?php echo "".$nameAdmin."";?></title>
<?php require '../../view/links/bootstrap.php';?> 
<link rel="stylesheet" href="../../view/css/style.css">
<link rel="shortcut icon" href="img/votacion.png" />
</head>
<body>
<header class="header">   
<nav class="navbar navbar-expand-lg fixed-top">
            <div class="container-fluid">
            <a class="navbar-brand"> <i><img src="../../view/img/escudo.png" alt="" width="40" height="40"></i></a>
            
                <ul class="nav navbar-nav pull-xs-right">
                        <li class="nav-item" id="name">
                             <?php echo '<a class="nav-link" href="#">'."  ".$nameAdmin. '</a>';?>
                        </li>
                </ul>
                <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="../../controllers/login/logout_admin.php"> 
                            <i class="fa fa-fw fa-user"></i>
                            Salir</a>
                        </li>
                </ul>
            </div>
    </nav> 
</header>