<?php require '../../models/register/nav_register.php';?>
<!-- view of the form to register a new votante -->
<div class="container-sm">
    <div class="card" id="register">
        <div class="card-header">
            <h3 class="card-title">Registrar votante</h3>
        </div>           
        <div class="card-body">
           <?php require '../../models/register/register_students.php'?>
        </div>
    </div>
</div>
</body>
</html>