<?php require 'nav_edit.php';?>
<!-- form view to edit candidate data -->
<div class="container">
    <div class="card" id="register">
        <div class="card-header">
            <h3 class="card-title">Editar datos del candidato</h3>
        </div>
            
        <div class="card-body">
        <?php require '../../models/institution/edit_candidate.php'?>
        </div>

    </div>
    <a type="submit" id="behind" href="../../view/candidate_inst.php?id=<?php echo "".$id."";?>" class="btn btn-raised btn-primary"><i class="fa fa-arrow-left"></i>  Atrás</a>  

</div>



</body>
</html>