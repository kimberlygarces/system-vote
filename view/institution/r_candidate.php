<?php require '../../models/register/nav_register.php';?>
<!-- view of the form to register a new candidate -->
<div class="container-sm">
    <div class="card" id="register">
        <div class="card-header">
            <h3 class="card-title">Registrar candidato</h3>
        </div>
        <div class="card-body">
           <?php require '../../models/register/register_candidate.php';?>
        </div>
    </div>
</div>
</body>
</html>