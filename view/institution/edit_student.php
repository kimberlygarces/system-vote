<?php require 'nav_edit.php';?>
    <!-- form view to edit assembly data -->
<div class="container">
    <div class="card" id="register">
        <div class="card-header">
            <h3 class="card-title">Editar datos del votante</h3>
        </div>
        <div class="card-body">
        <?php require '../../models/institution/edit_student.php'?>
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Importante!</strong>Solo Ingrese datos en nueva contraseña si la desea cambiar 
            </div>
        </div>
    </div>

<!-- <a type="submit" id="behind" href="../../controllers/institution/edit_assembly.php" class="btn btn-raised btn-primary"><i class="fa fa-arrow-left"></i>  Atrás</a>   -->

</body>

<script>
    $('#myAlert').on('closed.bs.alert', function () {
  // do something…
})
</script>
</html>