<?php
session_start();
$user=$_SESSION['admin'];

if (!isset($_SESSION['admin'])) {
    header("location:../index.html");
}

require '../../controllers/conection.php';
require '../../controllers/data.php';

$id=$_GET['id'];
// echo $id;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">

    <?php require '../links/bootstrap.php'?>

    <title>Editar</title>
</head>
<body>
<header class="header">
<nav class="navbar navbar-expand-lg fixed-top">

            <div class="container-fluid">
            <a class="navbar-brand"> <i><img src="../../view/img/escudo.png" alt="" alt="" width="40" height="40">
            </i> </a>
            <ul class="nav navbar-nav pull-xs-right">
               <li class="nav-item" id="name">
                    <!-- <?php echo '<a class="nav-link" href="#">'."  ".$nameAdmin. '</a>';?> -->
                </li>
            </ul>


  <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item">
                        <a class="nav-link" href="../../controllers/login/logout.php"><i class="fa fa-fw fa-user"></i> Salir</a>
                        </li>
                    </ul>
</nav>
</header>
<div class="container-sm">
 
    <div class="card" id="register">
        <div class="card-header">
            <h3 class="card-title">Editar datos de la Institución</h3>
        </div>
        <div class="card-body">
          <?php require '../../models/admin/edit_user.php'?>
          <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Importante!</strong> Debe ingresar nuevamente la contraseña asignada.
            </div>
          </div>
          
        </div>
        <a type="submit" id="behind" href="../../view/grup_admin.php?id=<?php echo "".$admin_id."";?>" class="btn btn-raised btn-primary"><i class="fa fa-arrow-left"></i>  Atrás</a>  

        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    
    </section>
    <!-- /.content -->
    
  </div>


                

</body>
</html>

