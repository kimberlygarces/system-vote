<?php
session_start();
$user=$_SESSION['admin'];
if (!isset($_SESSION['admin'])) {
    header("location:../index.html");
}
require '../../controllers/conection.php';
require '../../controllers/data.php';
$id =  $_GET["id"];
// echo $id;
?>

<!DOCTYPE html>
<html lang="es">
<head>
<title>SV <?php echo "".$name_institutions."";?></title>
<?php require '../../view/links/bootstrap.php';?> 
<link rel="stylesheet" href="../../view/css/style.css">
<link rel="shortcut icon" href="img/votacion.png" />


</head>
<body>

<header class="header">
    
<nav class="navbar navbar-expand-lg fixed-top">

            <div class="container-fluid">
            <a class="navbar-brand"> <i><img src="../../view/img/escudo.png" alt="" width="40" height="40"></i></a>
            
                <ul class="nav navbar-nav pull-xs-right">
                        <li class="nav-item" id="name">
                             <?php echo '<a class="nav-link" href="#">'."  ".$name_institutions. '</a>';?>
                        </li>
                </ul>
                <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="../../controllers/login/logout.php"> <i class="fa fa-fw fa-user"></i>Salir</a>
                        </li>
                </ul>
            </div>
    </nav> 
</header>
<!-- view of the form to register a new assembly -->

<div class="container">
<div class="row">
<div class="col-sm-6">

    <div class="card" id="register">
        <div class="card-header">
            <h3 class="card-title">Registrar categoria</h3>
        </div>
            
        <div class="card-body">
           <?php require '../../models/admin/category.php';?>
        </div>

    </div>
    </div>

<div class="col-sm-6">

    <div class="card" id="register">
        <div class="card-header">
            <h3 class="card-title">Lista de categorias</h3>
        </div>
            
        <div class="card-body">
           <?php require '../../models/admin/list_category.php';?>
        </div>

    </div>
    </div>
</div>

<a type="submit" id="behind" href="../../view/assembly_inst.php?id=<?php echo "".$admin_id."";?>" class="btn btn-raised btn-primary"><i class="fa fa-arrow-left"></i>  Atrás</a>  

</div>



</body>
</html>