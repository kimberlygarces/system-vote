<?php require 'nav_admin.php';?>
    

    <!-- form view to edit assembly data -->

<div class="container">
 
    <div class="card" id="register">
        <div class="card-header">
            <h3 class="card-title">Editar assamblea</h3>
        </div>
            
        <div class="card-body">
        <?php require '../../models/admin/edit_assembly.php'?>
        </div>

    </div>
</div>

</body>
</html>