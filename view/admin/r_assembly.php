<?php require 'nav_admin.php';?>
<!-- view of the form to register a new assembly -->
<div class="container-sm">
    <div class="card" id="register">
        <div class="card-header">
            <h3 class="card-title">REGISTRAR ASAMBLEA</h3>
        </div>
            
        <div class="card-body">
           <?php require '../../models/register/register_assembly.php';?>
        </div>

    </div>
</div>
</body>
</html>