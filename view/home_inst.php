<?php require 'template_user.php'?>
<div class="container">
<div class="row">        
<div class="col-sm-12">
        <div class="card-header">
        <?php require '../models/institution/home_inst.php' ?>
            <!-- Button trigger modal -->
      <hr>
      <a type="button" class="btn btn-secondary" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-list-ul"> </i>
        Crear categoría
      </a>
      <a type="button" class="btn btn-secondary" data-toggle="modal" data-target="#stadeAssembly"><i class="fas fa-certificate"></i>
        Estado de la Asamblea
      </a>
    </div>
</div>
<?php require '../models/institution/list__category.php';?>
</div>
</div>
</div>
</div>
<!-- Modal - crear nueva categorias -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-th-list" aria-hidden="true"></i> Crear nueva categoría</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php require '../models/institution/r_category.php';?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- modificar datos de la assamblea -->
<div class="modal fade" id="stadeAssembly" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Cambiar de estado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php require '../models/institution/stade_assembly.php';?>

    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php require 'template_footer.php'?>
