<?php require 'template_user.php';
$institution_id =  $_GET["id"];
$category = $_GET["category"];
?>


<div class="container-fluid">
<div class="col-sm-12">
        <div class="card-header">
        <?php require '../models/institution/home_inst.php' ?>
      <hr>
      <a type="button" class="btn btn-secondary" data-toggle="modal" data-target="#votoblanco"><i class="fas fa-certificate"></i>
        Voto en blanco
      </a>
      <?php
      echo '<a class="btn btn-secondary" href="institution/r_candidate.php?assemblycandidate='.$assembly_id.'&id='.$id_grupo.'">
      <i class="fas fa-user-tie"></i> Nuevo Candidato</a>';
      ?>

    </div>
 
      <div class="card">
        <div class="card-header">
            <h3 class="card-title">Candidatos de la categoría de <?php echo $category ?> </h3>
              </div>
              
                <div class="card-body">
                <?php require '../models/institution/specific_category.php' ?>
                </div>

      </div>
    </div>
<a type="submit" id="behind" href="home_inst.php?id=<?php echo "".$institution_id."";?>" class="btn btn-raised btn-primary"><i class="fa fa-arrow-left"></i>  Atrás</a>  

  </div>

</section>
<?php require 'template_footer.php'?>

<div class="modal fade" id="votoblanco" role="dialog">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Registrar voto en Blanco</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php require '../models/institution/blank_vote.php';?>

    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
