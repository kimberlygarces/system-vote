<?php
session_start();
$user=$_SESSION['auditor'];

if (!isset($_SESSION['auditor'])) {
    header("location:../../index.php");
}

require '../../controllers/conection.php';
require '../../controllers/data_auditor.php';


$id =  $_GET["id"];
// echo $id;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SV <?php echo "".$name_auditor."";?></title>
    <?php require '../links/bootstrap.php'?>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>

<header class="header">

<nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">

  <!-- Brand -->
  <a class="navbar-brand" href="#"><i><img src="../../view/img/escudo.png" alt="" alt="" width="40" height="40"></i></a>
   
    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse collapse"  id="collapsibleNavbar">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <a> <?php echo $name_institutions?> </a>
        </li>
    </div>
  <!-- Navbar links -->   
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2"  id="collapsibleNavbar">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <a class="nav-link" href="../../controllers/login/logout_auditor.php?inst=<?php echo "".$id_institutions."&id=".$id."";?>"><i class="fa fa-fw fa-user"></i> Salir</a>
            </li>
        
        </ul>
    </div>
</nav>
  
</header>
    <div class="container" id="control">

    <?php require '../../models/auditor/table_category.php'?>
        
    </div>
