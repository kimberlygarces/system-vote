<?php
session_start();
$user=$_SESSION['auditor'];

if (!isset($_SESSION['auditor'])) {
    header("location:../../index.php");
}

require '../../controllers/conection.php';
require '../../controllers/data_auditor.php';


$id =  $_GET["id"];
$id_institutions;
// echo $id;
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SV <?php echo "".$name_auditor."";?></title>
    <?php require '../links/bootstrap.php'?>
    <link rel="stylesheet" href="../../view/css/style.css">
    <link rel="shortcut icon" href="img/votacion.png" />

</head>
<body>

<header class="header">

<nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">

  <!-- Brand -->
  <a class="navbar-brand" href="#"><i><img src="../../view/img/escudo.png" alt="" alt="" width="40" height="40"></i></a>
   
    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse collapse"  id="collapsibleNavbar">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <a> <?php echo $name_institutions?> </a>
        </li>
    </div>
  <!-- Navbar links -->   
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2"  id="collapsibleNavbar">
        <ul class="navbar-nav ml-auto">

            <li class="nav-item">
            <a class="nav-link" href="../../controllers/login/logout_auditor.php?inst=<?php echo "".$id_institutions."&id=".$id."";?>"><i class="fa fa-fw fa-user"></i> Salir</a>
            </li>
        
        </ul>
    </div>
</nav>
  
</header>


<div class="container" id="control" >
<div class="row align-items-center">

    <div class="col-sm-12">  
        <?php  
        echo '<div class="card">';
        echo '<div class="card-header">',
        '<img src="../../view/img/auditor.png" alt="" width="50" height="60">',
        '  Datos del Auditor',
        '</div>';
        echo '<div class="card-body">'
        .$name_auditor.
        ' ',       
        '</div>';
     
        echo '</div>';
        
    ?>
    </div>

<div class="col-sm-12">  

        <?php  
        echo '<div class="card">';
    
        echo '<div class="card-body">',
        '<img src="data:'.$type.';base64,'.base64_encode($img_assembly).'" alt="" width="40" height="40">'
                ,' ASAMBLEA - '
                .$name_assembly.
                '</div>';    
        '</div>';
        echo '<div class="card-body">',
        '<a type="submit" class="btn btn-dark btn-rounded" href="../auditor/category.php?id='.$id_auditors.'&inst='.$id_institutions.'&assembly='.$id_assembly.'">
            Ver Resultado por categoria
        <i class="fa fa-arrow-right" aria-hidden="true"></i>
        </a>',
        '</div>';
        echo '<div class="card-header">',
        '<img src="../../view/img/reloj.png" alt="" width="40" height="40">',
        ' Vigente de '
        .$start_time.
        ' hasta '
        .$end_time.
        '</div>';
     
        echo '</div>';
        
    ?>

</div>

