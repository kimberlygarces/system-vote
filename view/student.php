<?php
session_start();
$user=$_SESSION['voter'];
if (!isset($_SESSION['voter'])) {
    header("location:../index.php");
}
require '../controllers/conection.php';
require '../controllers/data_students.php';
$id_student=$_GET['id'];
$id = $id_institutions;
?>
<!-- voter main view -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SV <?php echo "".$name_students."";?></title>
    <?php require 'links/bootstrap.php'?>
    <link rel="stylesheet" href="../view/css/style.css">
    <link rel="shortcut icon" href="img/votacion.png" />
</head>
<body>
<header class="header">
<nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">

  <!-- Brand -->
  <a class="navbar-brand" href="#"><i><img src="../view/img/escudo.png" alt="" alt="" width="40" height="40"></i></a>
   
    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse collapse"  id="collapsibleNavbar">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <a> <?php echo $name_institutions?> </a>
        </li>
    </div>
  <!-- Navbar links -->   
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2"  id="collapsibleNavbar">
        <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
            <a class="nav-link" href="student/candidate.php?inst=<?php echo "".$id."&id=".$id_student."";?>" ><i class="fa fa-fw fa-user"></i> Quiero ser candidato</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="../controllers/login/logout_student.php?inst=<?php echo "".$id."&id=".$id_student."";?>" ><i class="fa fa-fw fa-user"></i> Salir</a>
            </li>
        
        </ul>
    </div>
</nav>
  
</header>

<!-- voter data -->

<div class="container" id="control" >
<div class="row align-items-center">

    <div class="col-sm-12">  
        <?php  
        echo '<div class="card">';
        echo '<div class="card-header">',
        '<img src="../view/img/user.png" alt="" width=auto height="60">',
        '  Datos del votante',
        '<a type="submit" class="btn btn-light" data-toggle="modal" data-target="#exampleModalCenter">
        <i><img src="../view/img/view.png" alt="" width="20" height="20"></i> Mis datos
        </a>',
        '</div>';
        echo '<div class="card-body">'
        .$name_students.
        ' '
        .$last_students.
       
        '</div>';
     
        echo '</div>';
        
        ?>
                    
        </div>
    
        <div class="col-sm-12"> 
        <form action="student.php?id=<?php echo "".$id."";?>" method="post" class="">
        <?php     
        
        // function to verify quorun
        function validate($assembly,$student,$institution,$conn)
        {   
        $controls = "SELECT * FROM `quorum` 
        WHERE students_id = '" . $student. "'
        AND institution_id = '$institution'
        AND assembly_id = '$assembly'
        ";
        
        $control = $conn->query($controls);
           
        // var_dump($control->fetch_assoc()['assembly_id']);
        if ($control->num_rows > 0) {
            return $control->fetch_assoc()['assembly_id'];
        }
        return null;
        }

        $sql = "SELECT * FROM assemblies 
        WHERE institution_id = '$id_institutions'";

        $result = $conn->query($sql);
        while ($row = $result->fetch_assoc()) {
        $assembly_id = $row['assembly_id'];  
        $start_date = $row['start_date'];
        $end_date = $row['end_date'];
        $start_time = $row['end_time'];
        $end_time = $row['end_time'];
        $stade = $row['stade'];
        $type =$row['type'];
        $data_voter  =$row['data_voter'];
    
        date_default_timezone_set("America/Bogota"); //zona horaria seteada America/Bogota
        $mifecha = new DateTime(); 
        $hora = $mifecha->format('H:i:s');
        $hoy = $mifecha->format('Y-m-d');

    // $hora = date("H:i:s"); 
    // $hoy = date("Y-m-d"); 

        // si la assamblea esta inactiva
        if ($stade == "I") {
            echo '<div class="card">';
            echo '<div class="card-body">'
            .$row['name'].
            '</div>';
            echo '<div class="card-header">',
            '<img src="data:'.$type.';base64,'.base64_encode($row['img_assembly']).'" alt="" width=auto height="60">'
            ,' Asamblea - Inactiva '

            ,'</div>';
            echo '</div>';
        }else {   

            
            if(validate($assembly_id,$students_id,$id_institutions,$conn) == null && $end_date == $hoy && $end_time < $hora ){  
                echo '<div class="card">';
                echo '<div class="card-body">'
                .$row['name'].
                '</div>';
                echo '<div class="card-header">',
                '<img src="data:'.$type.';base64,'.base64_encode($row['img_assembly']).'" alt="" width="40" height="40">'
                ,'Registro de quorum cerrado '
                .$row['end_date'].
                ' a las '.$row['end_time'].
                '</div>';
                        echo '<div class="card-header">',
                '<a type="submit" class="btn btn-dark btn-rounded" href="../view/student/category.php?assembly='.$row["assembly_id"].'&id='.$students_id.'">
                ver categorias
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                </a>',
             '</div>';
                ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>El tiempo de registro de Quorum ha terminado! </strong> Puede ver las votaciones mas no podrá decidir
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
              <?php
                echo '</div>';

            }else {   
            // esta opcion sera visble no se inscribio en  la assamblea en el tiempo predispuesto
       if(validate($assembly_id,$students_id,$id_institutions,$conn) == null && $end_date < $hoy){  
            echo '<div class="card">';
            echo '<div class="card-body">'
            .$row['name'].
            '</div>';
            echo '<div class="card-header">',
            '<img src="data:'.$type.';base64,'.base64_encode($row['img_assembly']).'" alt="" width="40" height="40">'
            ,'Vencida  '
            .$row['end_date'].
            ' hora '.$row['end_time'].
            '</div>';
            ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <strong>Tiempo de Inscripción vencido!</strong> Esta asamblea ya paso la fecha de Inscripción
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
          <?php
            echo '</div>';
            
        }else{
            // if(validate($assembly_id,$students_id,$id_institutions,$conn) == null && $stade == "P"){  
            //     echo '<div class="card">';
            //     echo '<div class="card-body">',
            //     '<img src="data:'.$type.';base64,'.base64_encode($row['img_assembly']).'" alt="" width="auto" height="60">'
            //     .$row['name'].
            //     '</div>';
            //     echo '<div class="card-header">',
            //     '<a type="submit" class="btn btn-dark btn-rounded" href="../view/student/category.php?assembly='.$row["assembly_id"].'&id='.$students_id.'">
            //     ver categorias
            //     <i class="fa fa-arrow-right" aria-hidden="true"></i>
            //     </a>',
            //     '</div>';
                ?>
                <!-- <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Asamble activa, pero no se inscribió en el quórum a tiempo!</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div> -->
              <?php
            //     echo '</div>';
                
            // }else{
                 
            // esta opcion sera visble si la asamblea se esta preparando y ya esta registrado en el quorun
            if(validate($assembly_id,$students_id,$id_institutions,$conn) == $assembly_id and $stade == "P"){
                    
                echo '<div class="card">';
                echo '<div class="card-header">',
                '<img src="data:'.$type.';base64,'.base64_encode($row['img_assembly']).'" alt="" width="auto" height="60">'
                ,' ASAMBLEA - '
                .$row['name'].
                '</div>';
                echo '<div class="card-body">',
                '<a type="submit" class="btn btn-dark btn-rounded" href="../view/student/category.php?assembly='.$row["assembly_id"].'&id='.$students_id.'">
                   LA ASAMBLEA SE ESTA PREPARANDO
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                </a>',
                '</div>';
                echo '<div class="card-header">',
                '<img src="../view/img/reloj.png" alt="" width="auto" height="50">',
                ' Vigente de '
                .$row['star_time'].
                ' hasta '
                .$row['end_time'].
                '</div>';
                echo '</div>';
           
            }else{

                // esta opcion sera visible si la asamble esta activa y ya se registro en el quorun
            if(validate($assembly_id,$students_id,$id_institutions,$conn) and $stade == "A"){
                    
                echo '<div class="card">';
                echo '<div class="card-header">',
                '<img src="data:'.$type.';base64,'.base64_encode($row['img_assembly']).'" alt="" width="auto" height="70">'
                ,' ASAMBLEA - '
                .$row['name'].
                '</div>';
                echo '<div class="card-body">',
                '<a type="submit" class="btn btn-dark btn-rounded" href="../view/student/category.php?assembly='.$row["assembly_id"].'&id='.$students_id.'">
                    VER CATEGORIAS
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                </a>',
                '</div>';
                echo '<div class="card-header">',
                '<img src="../view/img/reloj.png" alt="" width="auto" height="30">',
                ' Vigente de '
                .$row['start_date'].
                ' hasta '
                .$row['end_date'].
                '</div>';
                echo '</div>';
           
            }else{
                
                //esta opcion sera visible si la asamblea se esta preparando y esta habilitada la fecha de requistro en el quorun
            if(validate($assembly_id,$students_id,$id_institutions,$conn) == null && $end_date >= $hoy){
                    echo '<div class="card">';
                    echo '<div class="card-header">',
                    '<img src="data:'.$type.';base64,'.base64_encode($row['img_assembly']).'" alt="" width="auto" height="60">'
                    ,' ASAMBLEA - '
                    .$row['name'].
                    '</div>';
                    echo '<div class="card-body">',
                    ' ',
                    '<a type="submit" class="btn btn-light" data-toggle="modal" data-target="#exampleModal">
                    <i><img src="../view/img/asistence.png" alt="" width="auto" height="50"></i>
                    Debe registrar su asistencia
                    </a>',
                    '</div>';
                    echo '<div class="card-header">',
                    '<img src="../view/img/reloj.png" alt="" width="auto" height="40">',
                    ' Vigente de '
                    .$row['start_date'].
                    ' hasta '
                    .$row['end_date'].
                    '</div>';
                    echo '</div>';
                    
            }
         
        }
    }
}
        }
    }
    }


        ?>
        </form>
        </div>
        </div>

        <div class="col-sm-12">  
        <?php  
        echo '<div class="card">';
        echo '<div class="card-header">',
        '<i class="fa fa-link" aria-hidden="true"></i>',
        ' Acceso a la Videoconferencia',
        '</div>';
        echo '<div class="card-body">',
        '<a type="submit" class="btn btn-warning"  target="_blank" href="../controllers/institution/videoconferencia.php?inst='.$id_institutions.'">
        <i class="fa fa-video-camera " aria-hidden="true" ></i>
        Videoconferencia
        </a>',
        '</div>';
     
        echo '</div>';
        
        ?>
                    
        </div>
        <!-- Modal assistance - quorun -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <?php require '../models/student/quorun.php'?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
                </div>
            </div>
            </div>

            <?php

            if($data_voter  == "A" ){

            ?>

            <!-- Modal modify voter data-->
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Modificar mis datos</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                        <?php require '../models/student/voter_data.php'?>
                        <br>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Importate!</strong> Cambie la contraseña de acceso al sistema
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                        </div>
                    </div>
                    </div>

                    <?php
                    }else {
                    ?>

                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Modificar mis datos</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                     
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Importate!</strong> NO SE HABILITO LA OPCIÓN PARA EDITAR CONTRASEÑA
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </div>
                        </div>
                    </div>
                    </div>

                    <?php
                    }
                    ?>
</body>
</html>

<script>
  //Cuando la página esté cargada completamente
  $(document).ready(function(){
    //Cada 10 segundos (1000 milisegundos) se ejecutará la función refrescar
    setTimeout(refrescar, 60000);
  });
  function refrescar(){
    //Actualiza la página
    location.reload();
  }
</script>