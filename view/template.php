<?php
session_start();
$user=$_SESSION['admin'];
if (!isset($_SESSION['admin'])){
  header("location:../index.php");
}
$id=$_GET['id'];
require '../controllers/conection.php';
require '../controllers/data.php';
?>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Sistema de votaciones</title>
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <link rel="shortcut icon" href="img/votacion.png" />

</head>

<style>

#sidebar{
 
  position: fixed;
    font-family: "Consolas", monospace;
    font-size: 11pt;

}

#content-wrapper{
  padding-top: 54px;
  text-align: center;

}

.card-header{
    font-family: "Consolas", monospace;
    font-size: 16pt;
    background: -moz-linear-gradient(270deg, rgb(2, 22, 199) 0%, rgb(86, 94, 206) 100%);
    background: -webkit-linear-gradient(270deg, rgba(0, 0, 0, 0.349) 0%, rgba(89, 78, 193, 0.336) 100%);
    border: none;
    border-radius: 3;    
}

.card{
  margin: 2%;

}

#register{
  text-align: left;
}
#register h4{
  font-family: "Consolas", monospace;
  text-align: center;
}



</style>

<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light fixed-top">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
      <a href="#" class="nav-link"></a>
      </li>
      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
    
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
      <a class="nav-link" href="../controllers/login/logout_admin.php?id=<?php echo "".$admin_id.""?>"> <i class="fa fa-fw fa-user"></i>Salir</a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside  id="sidebar"  class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Administrador</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="img/user_admin.png" alt="User Image">
        </div>
        <div class="info">
          <?php echo '<a class="d-block" href="#">'."  ".$nameAdmin. '</a>';?>
        </div>
      </div>
    

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <!-- menu para listar usuarios -->
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="#" class="nav-link active">
            <i class="fas fa-portrait"></i>
              <p>
                Instituciones
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a class="nav-link" href="new_user.php?id=<?php echo "".$id."";?>">
                  <p> <i class="fas fa-user-plus"></i> Registrar nueva</p>
                </a>
              </li>
              <li class="nav-item">
              <a class="nav-link" href="grup_admin.php?id=<?php echo "".$id."";?>">
                  <p> <i class="fa fa-list-ul"></i> Mis Instituciones</p>
                </a>
              </li>
        </ul>

             <!-- menu para listar usuarios -->
          <li class="nav-item">
            <a href="#" class="nav-link active">
            <i class="fas fa-boxes"></i>
               <p>
                Mis asambleas
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
            <!-- <li>
                <a class="nav-link" href="admin/r_assembly.php?id=<?php echo "".$id."";?>">
                <i class="fas fa-boxes"></i> Nueva Asamblea</a>
                </li> -->
                <li>
                <a class="nav-link" href="assembly_inst.php?id=<?php echo "".$id."";?>">
                <i class="fa fa-list-ul">  </i> Listar Asambleas</a>
                </li>
            </ul>

       
       <!-- menu para listar usuarios -->
       <li class="nav-item">
            <a href="#" class="nav-link active">
            <i class="fas fa-users"></i>
               <p>
                Votantes
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                <li>
                <a class="nav-link" href="list_voter.php?id=<?php echo "".$id."";?>">
                <i class="fa fa-list-ul">  </i> Lista de votantes</a>
                </li>

                <li>
                <a class="nav-link" href="list_quorun.php?id=<?php echo "".$id."";?>">
                <i class="fa fa-list-ul">  </i> Lista de quórum</a>
                </li>
            </ul>     
            
        <!-- opciones de configiración del sitio -->
        <li class="nav-item menu-open">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fas fa-cogs"></i>
              <p>
                Resultados
                <i class="right fas fa-angle-left"></i>
              </p>
          </a>
            <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="configure.php?id=<?php echo "".$id."";?>" class="nav-link">
                <i class="fas fa-file-signature"></i>          
                  <p>Mi sitio</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="control_admin.php?id=<?php echo "".$id."";?>" class="nav-link">
                <i class="fas fa-chart-pie"></i>          
                  <p>Consolidado</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="log_events.php?id=<?php echo "".$id."";?>" class="nav-link">
                <i class="fas fa-list-alt"></i>          
                  <p>Registro de eventos</p>
                </a>
              </li>
      </nav>
    </div>
  </aside>
  <section id="content-wrapper" class="content-wrapper">
