<?php
session_start();
$user=$_SESSION['institucion'];
if (!isset($_SESSION['institucion'])) {
    header("location:../index.html");
}
require '../controllers/conection.php';
require '../controllers/data_institution.php';

$id_grupo=$_GET['id'];
// echo $id;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SV <?php echo "".$name_institutions."";?></title>
    <link rel="stylesheet" href="css/style.css">

<?php require 'links/bootstrap.php'?>



<div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3>Sistema de Votaciones</h3>
        </div>

        <ul class="list-unstyled components">
            <p>Bienvenido</p>
        <li class="active" id="active">
        <!-- <i class="fa fa-home"></i>  -->
        <a id="p" href="#">Mi Perfil </a>
        <a id="p" href="#homeSubmenuA" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Mis asambleas</a>
            <ul class="collapse list-unstyled" id="homeSubmenuA">
                <li>
                <a class="nav-link" href="institution/r_assembly.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fas fa-user-plus"></i> Nueva Asamblea</a>
                </li>
                <li>
                <a class="nav-link" href="assembly_inst.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fa fa-list-ul"></i> Listar Asambleas</a>
                </li>
            </ul>
           
            <a id="p" href="#homeSubmenuC" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Mis candidatos</a>
            <ul class="collapse list-unstyled" id="homeSubmenuC">
                <li>
                <a class="nav-link" href="institution/r_candidate.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fas fa-user-plus"></i> Nuevo candidato</a>
                </li>
                <li>
                <a class="nav-link" href="candidate_inst.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fa fa-list-ul"></i> Lista de candidatos</a>
                </li>

            </ul>
            <a id="p" href="#homeSubmenuV" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Votantes</a>
            <ul class="collapse list-unstyled" id="homeSubmenuV">
                <li>
                <a class="nav-link" href="institution/r_votante.php?id=<?php echo "".$id_grupo."";?>">
                <i class="fas fa-user-plus"></i> Nuevo votante</a></li>
                <li>
                <a class="nav-link" href="students_inst.php?id=<?php echo "".$id_grupo."";?>">
                <!-- <a class="nav-link" href="" onclick="voter()"> -->
                <i class="fa fa-list-ul"></i> Lista de votantes</a></li>
            </ul>
            <ul class="list-unstyled CTAs">
                <li>
                <a href="control.php?id=<?php echo "".$id_grupo."";?>" class="download">
                <i class="fa fa-tasks" aria-hidden="true"></i>
                 Resultados especificos</a></li>
                </a>
                </li>
             </ul>
          
             <ul class="list-unstyled CTAs">
                <li>
                <a href="resultassembly.php?id=<?php echo "".$id_grupo."";?>" class="download">
                <i class="fa fa-check" aria-hidden="true"></i>
                Resultados Generales</a></li>
                </a>
                </li>
             </ul>
             
            <ul class="list-unstyled CTAs">
                <li>
                <a href="control_inst.php?id=<?php echo "".$id_grupo."";?>" class="download">
                <i class="fa fa-check" aria-hidden="true"></i>
                Votantes activos</a></li>
                </a>
                </li>
             </ul>

    </nav>

    <!-- Page Content Holder -->
    <div id="content">
        <nav class="navbar navbar-expand-lg">

            <div class="container-fluid">
                <button type="button" id="sidebarCollapse" class="navbar-btn">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-align-justify"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav pull-xs-right">
                    <li class="nav-item">
                        <a class="nav-link">
                        <i><img src="img/escudo.png" alt="" width="70" height="60">
                        <!-- <?php echo '<a>'."  ".$name_institutions. '</a>';?> -->
                        </i> </a>
                    </li>
                        <li class="nav-item" id="name">
                             <?php echo '<a class="nav-link" href="#">'."  ".$name_institutions. '</a>';?>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="../controllers/login/logout.php"> <i class="fa fa-fw fa-user"></i>Salir</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </head>
<body>

<div class="container">

</div>
 
    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $(this).toggleClass('active');
            });
    }); 

    
 
     
   </script>

<script>
        $(document).ready(function(){
        $('[data-toggle="popover"]').popover();   
        });

      
</script>

</body>
</html>