<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SV <?php echo "".$name_students."";?></title>
    <?php require '../links/bootstrap.php'?>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="shortcut icon" href="../img/votacion.png" />

</head>
<body>

<header class="header">
<nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
  <!-- Brand -->
  <a class="navbar-brand" href="#"><i><img src="../../view/img/escudo.png" alt="" alt="" width="40" height="40"></i></a>  
    <!-- Toggler/collapsibe Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse collapse"  id="collapsibleNavbar">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <a> <?php echo $name_institutions?> </a>
        </li>
    </div>
  <!-- Navbar links -->   
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2"  id="collapsibleNavbar">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <a class="nav-link" href="../../controllers/login/logout_student.php?id=<?php echo "".$id."&inst=".$id_institutions."";?>" ><i class="fa fa-fw fa-user"></i> Salir</a>
            </li>
        
        </ul>
    </div>
</nav>
  
</header>