<?php require 'template.php'?>

<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                  <h3 class="card-title">Listado de votantes habilitados</h3>
                    </div>
                    
                      <div class="card-body">
                      <?php require '../models/admin/list_voter.php';?>
                      </div>

            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <?php require 'template_footer.php'?>
