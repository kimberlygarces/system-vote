<?php require 'template.php'?>

<div class="container-sm">
 
    <div class="card" id="register">
        <div class="card-header">
            <h3 class="card-title">Registrar Institución</h3>
        </div>
            
        <div class="card-body">
        <?php require '../models/register/register_institution.php'?>
        </div>
    </div>
    </div>
    </div>

<?php require 'template_footer.php'?>

    
  