<?php
// cerrar sesion para el administrador
    session_start();
    require '../log_events.php';

    $user=$_SESSION['admin'];
    $admin = $_GET['id'];
        // se crea evento que registra el cierre de la sesión de user
    $events = 'CIERRE DE SESIÓN '.$user.'';
    $user = $user;
    $id =  $admin;
    events_log($id,$user,$ipAdress,$events,$conn);
    unset($user);
    header("Location: ../../index.php");
?>