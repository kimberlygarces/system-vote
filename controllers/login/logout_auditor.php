<?php
    // cerrar sesion para el auditor
    session_start();
    $user=$_SESSION['auditor'];
    $auditor = $_GET['id'];
    $institution = $_GET['inst'];
    require '../log_events.php';
    // se crea evento que registra el cierre de la sesión de user
    $events = 'CIERRE DE SESIÓN '.$user.'';
    $user = $user;
    $id =  $institution;
    events_log($id,$user,$ipAdress,$events,$conn);
    unset($user);
    header("Location: ../../index.php");
?>