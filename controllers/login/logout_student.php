<?php
    // cerrar sesion para el votante
    session_start();
    $user=$_SESSION['voter'];
    $student = $_GET['id'];
    $institution = $_GET['inst'];
    require '../log_events.php';
    // se crea evento que registra el cierre de la sesión de user
    $events = 'votante - CIERRE DE SESIÓN '.$student.'';
    $user = $student;
    $id =  $institution;
    events_log($id,$user,$ipAdress,$events,$conn);
    unset($user);
    header("Location: ../../index.php");

?>