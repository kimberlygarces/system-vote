<?php
session_start();
$user=$_SESSION['voter'];
if (!isset($_SESSION['voter'])) {
    header("location:../../index.php");
}

require '../conection.php';
require '../data_students.php';
require '../log_events.php';

$student = $_GET['id'];
$id_institutions;
$id_assembly;

$document_voter= $_POST['document'];

//funcion para validar si el votante ya esta inscrito en el quorum
  function validate($id_assembly,$student,$id_institutions,$conn)
        {   
        $controls = "SELECT * FROM `quorum` 
        WHERE students_id = '" . $student. "'
        AND institution_id = '$id_institutions'
        AND assembly_id = '$id_assembly'
        ";
        
        $control = $conn->query($controls);
           
        // var_dump($control->fetch_assoc()['assembly_id']);
        if ($control->num_rows > 0) {
            return $control->fetch_assoc()['assembly_id'];
        }
        return null;
        }
// si existe no podra registrase y se registrara el log - intento de quorum duplicado
if(validate($id_assembly,$students_id,$id_institutions,$conn) == $id_assembly){

    $events = 'Votante - INTENTO DUPLICADO DE QUORUN ';
    $user = $user;
    $id =  $id_institutions;
    events_log($id,$user,$ipAdress,$events,$conn);

header("Location: ../../view/student.php?id=$student");

// si no existe se registrara en quorun sin problema
}elseif (validate($id_assembly,$students_id,$id_institutions,$conn) == null) {
    // echo 'registro ok';

$resultado = $conn->query('SELECT * FROM  `students` WHERE ( document  = "'.$document_voter.'")');
$filas = $resultado->num_rows;

$registro = $resultado->fetch_assoc();
if ($filas == !0) {   
    $electoral_quotient = $registro['electoral_quotient'];

    $sql = "INSERT INTO `quorum`(`institution_id`, `assembly_id`, `students_id`, `date`, `time`, `document_voter`, `electoral_quotient`)
    VALUES ('".$id_institutions."','".$id_assembly."','".$student."',CURDATE(),CURRENT_TIMESTAMP(),'".$document_voter."','".$electoral_quotient."')";

    if($result =  $conn->query($sql)){

        $events = 'Votante - REGISTRO EN EL QUÓRUM';
        $user = $user;
        $id =  $id_institutions;
        events_log($id,$user,$ipAdress,$events,$conn);

        echo '<script language="javascript">
        window.location.href=" ../../view/student.php?id='.$student.'"</script>';
    }else{

        $events = 'Votante - ERROR- REGISTRO EN EL QUÓRUM';
        $user = $user;
        $id =  $id_institutions;
        events_log($id,$user,$ipAdress,$events,$conn);
        
        echo '<script language="javascript">alert("Algo salio mal");
        window.location.href=" ../../view/student.php?id='.$student.'"</script>';
    }

}else{

    $events = 'Votante - ERROR- REGISTRO EN EL QUÓRUM';
    $user = $user;
    $id =  $id_institutions;
    events_log($id,$user,$ipAdress,$events,$conn);
    
    echo '<script language="javascript">alert("El número de identificación no coincide con el registrado");
    window.location.href=" ../../view/student.php?id='.$student.'"</script>';    
       
}
}



?> 