<?php
$candidate =  $_GET["candidate"];
$assembly =  $_GET["assembly"];
$student = $_GET['id'];
$category_candidate = $_GET['category'];

// echo $user;
require '../conection.php';
require '../log_events.php';
require '../data_students.php';

$id_institutions;

// funcion para validar si el voto ya esta registrado
function validate($assembly,$student,$institution,$category,$conn)
        {   
        $controls = "SELECT * FROM `controls` 
        WHERE students_id = '" . $student. "'
        AND institution_id = '$institution'
        AND assembly_id = '$assembly'
        AND category = '$category'";
        $control = $conn->query($controls);
           
        // var_dump($control->fetch_assoc()['assembly_id']);
        if ($control->num_rows > 0) {
            return $control->fetch_assoc()['category'];
        }

        return null;

  }
//realizamos consulta para obtener datos especificos del candidato
$sql = "SELECT * FROM `candidates` WHERE candidate_id = '" . $_GET["candidate"] . "'";

if ( $result = $conn->query($sql) ){
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {

            $id_institutions= $row["institution_id"];
            $assembly_id = $row["assembly_id"];
            $category_candidate = $row["category"];
        }
        
    } else { 
        $row= "0 results"; 
    }
} else {
        $row="Error en la consulta: ".$conn->error;
}

$sql = "SELECT * FROM `category` WHERE name = '" . $_GET["category"] . "'";

if ( $result = $conn->query($sql) ){
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {

           $stade_category= $row["stade"];
          
        }
        
    } else { 
        $row= "0 results"; 
    }
} else {
        $row="Error en la consulta: ".$conn->error;
}


//se realiza consulta para obtener los datos del votante
$sql = "SELECT * FROM `students` WHERE students_id  = '" . $_GET["id"] . "'";

if ( $result = $conn->query($sql) ){
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {

            $students_id = $row["students_id"];
            $electoral_quotient = $row["electoral_quotient"];
        }
        
    } else { 
        $row= "0 results"; 
    }
} else {
        $row="Error en la consulta: ".$conn->error;
}

$sql = "SELECT * FROM `controls` WHERE students_id = '" . $student. "'";
if ( $result = $conn->query($sql) ){
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {

            $id_control= $row["institution_id"];
            $name_control = $row["institution_id"];
            $student_control = $row["students_id"];
            $assembly_control = $row["assembly_id"];
            $date_control = $row["date"];
            $activation_control = $row["system_activation"];
        }
    } else { 
        $row= "0 results"; 
    }
} else {
        $row="Error en la consulta: ".$conn->error;
}
  

$electoral_quotient = $electoral_quotient;
$system_activation = "A";

if(validate($assembly_id,$students_id,$id_institutions,$category_candidate,$conn) == $category_candidate){//Validar si los datos de votante y categoria ya estan registrando - evitando duplicar votos
   
    $events = 'Votante - INTENTO DE DUPLICAR VOTO';
    $user = $student;
    $id =  $student;
    events_log($id,$user,$ipAdress,$events,$conn);
    echo "Estas intentando duplicar el voto ";
    header("Location:  ../../view/student.php?id=$students_id");
}else{
    if($stade_category != "A"){// verifica el estado de la categoria este activo antes de registrar el voto
    //echo 'NO se puede realizar registro';
    $events = 'Votante - INTENTO DE VOTO DURANTE CATEGORIA CERRADA';
    $user = $student;
    $id =  $id_institutions;
    events_log($id,$user,$ipAdress,$events,$conn);
    header("Location:  ../../view/student.php?id=$students_id");

    }else{// si el estado es A el voto es registrado
            //se insertan los datos en control de votos
            $sqlcontrol = "INSERT INTO `controls`(`institution_id`, `students_id`, `assembly_id`, `date`, `time`, `system_activation`, `category`, `electoral_quotient`) 
            VALUES ('".$id_institutions."','".$student."','".$assembly_id."',CURDATE(),CURRENT_TIMESTAMP(),'".$system_activation."','".$category_candidate."','".$electoral_quotient."')";
            if($result =  $conn->query($sqlcontrol)){
                $events = 'Votante - REGISTRADO EN EL CONTROL DE VOTOS '.$assembly_id.'';
                $user = $student;
                $id =  $id_institutions;
                events_log($id,$user,$ipAdress,$events,$conn);
                $votes = $electoral_quotient;
            //Se insertan los datos del voto
            $sql = "INSERT INTO `votes`(`candidate_id`, `assembly_id`, `institution_id`, `date`, `total_votes`)
            VALUES ('".$candidate."','".$assembly_id."','".$id_institutions."',CURDATE(),'".$votes."')";
            if($result =  $conn->query($sql)){
                $events = 'VOTO REGISTRADO EN LA ASSAMBLEA '.$assembly_id.'';
                $user = $student;
                $id =  $id_institutions;
                events_log($id,$user,$ipAdress,$events,$conn);
                header("Location:  ../../view/student/category.php?id=$students_id&assembly=$assembly");

                // echo '<script language="javascript">
                // window.location.href=" ../../view/student/category.php?assembly='.$assembly_id.'&id='.$students_id.'"</script>';
                //echo "OK ";
            }else{
            
                $events = 'Votante - ERROR - EN REGISTRO DE VOTO';
                $user = $student;
                $id =  $id_institutions;
                events_log($id,$user,$ipAdress,$events,$conn);
                header("Location:  ../../view/student.php?id=$students_id");

                // echo '<script language="javascript">alert("verifique los datos ingresados");
                // window.location.href=" ../../view/student.php?id='.$students_id.'"</script>';
            //echo "verifique los datos ingresados ";
                        
                    }
                }
    }    
}
    

?> 