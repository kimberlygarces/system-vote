<?php
require 'controllers/conection.php';

require('pdf/fpdf.php');
$pdf = new FPDF();
$pdf->AddPage('PORTRAIL','letter');
$id = $_GET["ok"];

class PDF extends FPDF
{
   //Cabecera de página
   function Header()
   {

       $this->Image('view/img/jcc-logo.png',18,8,80);

      $this->SetFont('Arial','B',12);

      // $this->Cell(190,20,'Inscripciones para el proceso de elecciones',1,0,'C');
      

   }

   function Footer()
        {

        $this->SetY(-20);

        $this->SetFont('Arial','I',8);

        $this->Cell(0,10,'Bogota Colombia ',0,0,'C');
        }

}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AddPage();
$pdf->SetFont('Times','B',18);
$pdf->Ln(25);
$pdf->Cell(48,48,'',0);

$pdf->Cell(100,10,utf8_decode("Comprobante de Inscripcion para el proceso de elecciones de miembros"),0,0,'C');
$pdf->Ln(8);
$pdf->Cell(48,48,'',0);

$pdf->Cell(100,10,utf8_decode("del tribunal Disciplinario de la Junta Central de Contadores"),0,0,'C');
$pdf->Ln(10);
$pdf->Cell(48,48,'',0);
$pdf->Cell(100,10,utf8_decode("2022 - 2025"),0,0,'C');
$pdf->Ln(10);
$pdf->SetFont('Times','',12);
$pdf->Cell(0,10,utf8_decode('Fecha de inscripción: ').date('d-m-Y').'',0);

$pdf->Ln(15);
$pdf->SetFont('Times','B',12);
$pdf->Cell(100,8,'DATOS REGISTRADOS',0);

 $sql = "SELECT * FROM candidates WHERE document ='$id'";
 
        $resultado = $conn->query($sql);
        while ($registro = $resultado->fetch_assoc()) {
    $pdf->SetFont('Times','',12);
      $pdf->Ln(12);
   
      $pdf->Cell(60,8,"Primer Nombre",1);
      $pdf->Cell(120,8,$registro['name'],1);
      $pdf->Ln();
      $pdf->Cell(60,8,"Segundo Nombre ",1);
      $pdf->Cell(120,8,$registro['name2'],1);
      $pdf->Ln();
      $pdf->Cell(60,8,"Primer apellido",1);
      $pdf->Cell(120,8,$registro['last'],1);
      $pdf->Ln();
      $pdf->Cell(60,8,"Segundo Apellido",1);
      $pdf->Cell(120,8,$registro['last2'],1);
      $pdf->Ln();
      $pdf->Cell(60,8,"Correo Electronico",1);
      $pdf->Cell(120,8,$registro['correo'],1);
      $pdf->Ln();
      $pdf->Cell(45,8,utf8_decode("Cedula"),1);
      $pdf->Cell(45,8,$registro['document'],1);
      $pdf->Cell(45,8,utf8_decode("Fecha de expedición"),1);
      $pdf->Cell(45,8,$registro['document_date'],1);
      $pdf->Ln();
      $pdf->Cell(45,8,utf8_decode("Tarjeta profesional"),1);
      $pdf->Cell(45,8,$registro['professional_card'],1);
      $pdf->Cell(45,8,utf8_decode("Autorizado o Titilado"),1);
      $pdf->Cell(45,8,$registro['AT'],1);
      $pdf->Ln();
      $pdf->Cell(60,8,utf8_decode("Inscrito para: "),1);
      $pdf->Cell(120,8,$registro['category'],1); 
       $pdf->Ln(12);
    }
    $pdf->SetFont('Times','B',12);
   


        $pdf->Ln(35);
        // $pdf->Image('assets/img/c.png',150,250,50);
        // $pdf->SetFont('Times','B',12);
        // $pdf->Cell(100,8,'______________________________',0);
        // $pdf->Ln();
        // $pdf->Cell(100,8,'Firma',0);

$pdf->Output(); ?>
